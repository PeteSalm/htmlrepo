$(document).ready(function() {
	
	
	var c = document.getElementById("gameCanvas");
	c.style.outline = "none";
	
	var context = c.getContext("2d");
	
	/* LOAD RESOURCES */
	

	var imagearray = {};
	var soundShot = {};
	var soundShell = {};
	var soundLaser = {};
	var soundDroid = {};
	var soundGrunt = {};
	var totalResources = 254;
	var numResourcesLoaded = 0;
	var fps = 30;
	
	/* LOAD SOUNDS */ 
	
	for (i = 0; i < 20; i = i + 1) {
		soundShot[i] = new Audio("assets/sounds/pistolshot.wav");
		soundShot[i].volume = 0.1;
		soundShot[i].autoplay = false;
		soundShot[i].loop = false;
	}
	
	for (i = 0; i < 20; i = i + 1) {
		soundShell[i] = new Audio("assets/sounds/shellfall.wav");
		soundShell[i].volume = 0.1;
		soundShell[i].autoplay = false;
		soundShell[i].loop = false;
	}
	
	for (i = 0; i < 20; i = i + 1) {
		soundLaser[i] = new Audio("assets/sounds/laserblast.mp3");
		soundLaser[i].volume = 0.1;
		soundLaser[i].autoplay = false;
		soundLaser[i].loop = false;
	}
	
	for (i = 0; i < 20; i = i + 1) {
		soundDroid[i] = new Audio("assets/sounds/hitdroid.mp3");
		soundDroid[i].volume = 0.1;
		soundDroid[i].autoplay = false;
		soundDroid[i].loop = false;
	}
	
	for (i = 0; i < 20; i = i + 1) {
		soundGrunt[i] = new Audio("assets/sounds/grunt.mp3");
		soundGrunt[i].volume = 0.1;
		soundGrunt[i].autoplay = false;
		soundGrunt[i].loop = false;
	}
	
	var bgmTrack = new Audio("assets/sounds/bgmTrack1.mp3");
	bgmTrack.volume = 0.1;
	bgmTrack.autoplay = false;
	bgmTrack.loop = true;
	
	/* LOAD IMAGES */
	
	function loadImage(name) {
		imagearray[name] = new Image();
		imagearray[name].onload = function() {
			resourceLoaded();
		}
		imagearray[name].src = "assets/images/all/"+name+".png";
	}
	
	loadImage("enemydroid");
	loadImage("projectile");
	
	loadImage("menu");
	loadImage("HUD");
	loadImage("map1");
	loadImage("chcursor");
	
	loadImage("charstand1");
	loadImage("invcharstand1");
	loadImage("armlessstand");
	loadImage("invarmlessstand");
	loadImage("gunarm");
	loadImage("invgunarm");
	for (i = 0; i < 11; i = i+1) {
		loadImage("gunarms"+i);
		loadImage("invgunarms"+i);
		loadImage("gunarmsreverse"+i);
		loadImage("invgunarmsreverse"+i);
	}
	for (i = 0; i < 10; i = i+1) {
		loadImage("charwalk"+i);
		loadImage("invcharwalk"+i);
		loadImage("charcrouch"+i);
		loadImage("invcharcrouch"+i);
		loadImage("pistolshoot"+i);
		loadImage("invpistolshoot"+i);
		loadImage("armlesscharwalk"+i);
		loadImage("invarmlesscharwalk"+i);
		loadImage("armlesscharcrouch"+i);
		loadImage("invarmlesscharcrouch"+i);
	}
	for (i = 0; i < 15; i = i+1) {
		loadImage("jump"+i);
		loadImage("invjump"+i);
	}
	for (i = 0; i < 28; i = i+1) {
		loadImage("jumpclimbtest"+i);
		loadImage("invjumpclimbtest"+i);
	}
	for (i = 0; i < 6; i = i+1) {
		loadImage("sfxblood"+i);
		loadImage("invsfxblood"+i);
	}
	
	/* LOGIN DIALOG */
	
	var dialog = $( "#dialog-form" ).dialog({
		autoOpen: true,
		height: 350,
		width: 350,
		modal: true,
		buttons: {
			Submit: function() {
				var user = $("#username").val();
				var pass = $("#password").val();
				if ((user.length > 0 && user.length < 9) && (pass.length > 0 && pass.length < 9)) {
				pDataArray = ["userinfo", user, pass];
				socket.emit("send message", pDataArray);
				pDataArray = [];
				var exists = 0;
				var userpass = '{"username":"'+user+'","password":"'+pass+'"}';
				socket.on("rows", function(rows) {
					exists = 0;
					for (i = 0; i< rows.msg.length; i = i + 1) {
						if (userpass === JSON.stringify(rows.msg[i])) {
							console.log("Logging in...");
							exists = 2;
							break;
						} else if (userpass.slice(0,userpass.indexOf(',"password":')) === JSON.stringify(rows.msg[i]).slice(0,userpass.indexOf(',"password":')) ) {
							console.log("Username exists already / password is wrong!");
							window.alert("Username exists already / password is wrong!");
							$("#username").val("");
							$("#password").val("");
							exists = 1;
							break;
						}
					}
					if (exists === 0) {
						console.log("Creating new user...");
						pDataArray = ["newuser", user, pass];
						socket.emit("send message", pDataArray);
						exists = 2;
						
					}
					if (exists === 2) {
						socket.emit("send message", ["getuserdata",user]);
						socket.on("userdata", function(userdata) {
							var t1 = JSON.stringify(userdata.msg);
							clientUsername = t1.slice(t1.indexOf('username":')+11, t1.indexOf('password":')-3);
							if (clientUsername === user) {
								userdataparser(JSON.stringify(userdata.msg));
							}
						});
						exists = 0;
						dialog.dialog("close");
					}
				});
				} else {
					window.alert("Password and username must be between 1-8 characters long!");
				}
			}
		}
    });
	
	var clientUsername = "";
	var clientGamename = "";
	var clientLevel = 0;
	var clientXp = 0;
	var clientAgi = 0;
	var clientAcc = 0;
	var clientJump = 0;
	var clientPoints = 0;
	
	
	function getHighscores() {
		var scores = [];
		var scorers = [];
		var scoresTemp = [];
		var numbers = [];
		var id = 0;
		socket.emit("send message", ["gethighscores"]);
		socket.on("highscores", function(highscores) {
			var hsArray = JSON.stringify(highscores.msg);
			for (i = 0; i < highscores.msg.length; i = i + 1) {
				var data = JSON.stringify(highscores.msg[i]);
				var t1 = data;
				var t2 = data;
				scorers[i] = t1.slice(t1.indexOf('username":')+11, t1.indexOf('score":')-3);
				scoresTemp[i] = [i, t2.slice(t2.indexOf('score":')+7, t2.indexOf('}'))];
			}
			
			for (i = 0; i < scoresTemp.length; i++) {
				scores.push(scoresTemp[i][1]);
				numbers.push(scoresTemp[i][0]);
			}
			scores.sort(sortNumber);
			var counter = 1;
			document.getElementById("leaderboards").innerHTML = '<p id="statsheader" style="margin-left: 20px">Highscores:</p>';
			for (i = 0; i < scores.length; i++) {
				for (j = 0; j < scoresTemp.length; j++) {
					if (scores[i] === scoresTemp[j][1]) {
						id = scoresTemp[j][0];
						if (counter < 6) {
							document.getElementById("leaderboards").innerHTML += '<p class="stats" style="margin-left: 20px" overflow="auto">'+counter+': '+scorers[id]+' - '+scores[i]+'</p>';
						}
						counter = counter + 1
					}
				}
			}
			
			
			function sortNumber(a,b) {
				return b - a;
			}
		});
	}
	
	/* USERDATAPARSER */
	
	function userdataparser(userdata) {
		
		if (userdata.length < 8) {
			window.alert("Error when logging in! Try again!")
			location.reload();
		}
		
		var t1 = userdata;
		var t2 = userdata;
		var t3 = userdata;
		var t4 = userdata;
		var t5 = userdata;
		var t6 = userdata;
		var t7 = userdata;
		var t8 = userdata;
		
		clientUsername = t1.slice(userdata.indexOf('username":')+11, userdata.indexOf('password":')-3);
		clientGamename = t2.slice(userdata.indexOf('gamename":')+11, userdata.indexOf('level":')-3);
		clientLevel = t3.slice(userdata.indexOf('level":')+7, userdata.indexOf('xp":')-2);
		clientXp = t4.slice(userdata.indexOf('xp":')+4, userdata.indexOf('ptsAgi":')-2);
		clientAgi = t5.slice(userdata.indexOf('ptsAgi":')+8, userdata.indexOf('ptsAcc":')-2);
		clientAcc = t6.slice(userdata.indexOf('ptsAcc":')+8, userdata.indexOf('ptsJump":')-2);
		clientJump = t7.slice(userdata.indexOf('ptsJump":')+9, userdata.indexOf('ptsPnt":')-2);
		clientPoints = t8.slice(userdata.indexOf('ptsPnt":')+8, userdata.indexOf('}]'));
		
		clientLevel = parseInt(clientLevel);
		clientXp = parseInt(clientXp);
		clientAgi = parseInt(clientAgi);
		clientAcc = parseInt(clientAcc);
		clientJump = parseInt(clientJump);
		clientPoints = parseInt(clientPoints);
		
		
		document.getElementById("clientLevel").innerHTML = "Level: "+clientLevel;
		document.getElementById("clientXp").innerHTML = "XP: "+clientXp;
		document.getElementById("clientAgi").innerHTML = "Agility: "+clientAgi;
		document.getElementById("clientAcc").innerHTML = "Accuracy: "+clientAcc;
		document.getElementById("clientJump").innerHTML = "Jump: "+clientJump;

	}
	
	/* START WHEN RESOURCES ARE LOADED */
	function resourceLoaded() {
		numResourcesLoaded += 1;
		if (numResourcesLoaded === totalResources) {
			document.getElementById("loader").style.zIndex = "-1";
			setInterval(redraw, 1000 / fps);
			document.getElementById("menustage11").style.display = "block";
			document.getElementById("menustage12").style.display = "block";
			
		}
	}
	
	
	/* GAMEFUNCTIONS BEGIN */
	
	/* EVENT LISTENERS */
	
	c.addEventListener("keydown", keyPressEvent);
	c.addEventListener("keyup", keyUpEvent);
	c.addEventListener("mousedown", rightMouseDownEvent);
	c.addEventListener("click", leftMouseClickEvent);
	c.addEventListener("mousemove", mouseMoveEvent);
	c.addEventListener("mouseup", rightMouseUpEvent);
	
	/* DEFINE GAMEVARIABLES */
	
	var socket = io.connect();
	
	var aimCorrectA = 0;
	var aimCorrectB = 0;
	var aimCorrectC = 0;
	var aimCorrectG = 0;
	var aimCorrect = 0;	
	
	var keyPressed = [null];
	var walkloop = 0;
	var crouchloop = 0;
	var jumploop = -1;
	var gundrawloop = 0;
	var shootloop = 10;
	var faceDirection = "left";
	var standStatus = "stand";
	var aimStatus = false;
	var shootStatus = false;
	var aimAngle = 0;
	var mouseDistance = 0;
	var rmbDown = false;
	var zoomAmount = 0.7;
	var groundY = 450;
	var airtime = 0;
	var aimTemp = false;

	var bg = imagearray["map1"];
	var bgX = 0;
	var bgY = 50;
	var difference = 0;
	var newPosX = 0;
	var oldPosX = 0;
	var cameraX = 0;
	var cameraY = 0;
	var unfocus = false;
	var focusValue = 10;
	var focusDelta = 0;
	
	var crouchingX = [186,185,182,177,164,157,147,144,146,149];
	var crouchingY = [89,93,99,109,135,172,185,186,183,182];
	var invcrouchingX = [200,200,201,202,217,225,232,238,234,230];
	var invcrouchingY = [89,95,101,112,136,175,184,182,182,183];
	var midcrouchingX = [5,5,5,0,-20,-20,-33,-37,-34,-32];
	var midcrouchingY = [7,10,22,25,54,89,102,107,104,104];
	var invmidcrouchingX = [-11,-9,-5,2,21,22,36,28,35,36];
	var invmidcrouchingY = [5,13,19,30,57,92,102,105,106,101];
	
	var map1x = [0,      50, 1720,   50,  50,   50,   50,  850,  830, 1730, 2580, 2580, 2580, 2580, 2580, 1720];
	var map1l = [3000, 1520,  750, 1520, 660, 2400,  660, 1600,  660,  660,  500,  500,  500,  500 , 500,  750];
	var map1y = [1940,  395,  405,  685, 995, 1285, 1600,  995, 1600, 1600,  408,  693,  995, 1285, 1600,  690];
	var map1 = [map1x, map1l, map1y];
	var maps = [map1];
	
	var xpCeilings = [0,100,300,700,1400,2000,3000,5000,7000,10000,14000,20000];
	
	var pCharX = c.width/2 - imagearray["invcharwalk"+walkloop].naturalWidth*zoomAmount/2;
	var pCharY = 50;
	var mousePosX = 0;
	var mousePosY = 0;
	var pHealth = 1;
	
	var pDataArray = [];
	var eDataArray = [];
	var eCharX = 0;
	var eCharY = 0;
	var estandStatus = "stand";
	var efaceDirection = "left";
	var eaimStatus = false;
	var emousePosX = 0;
	var emousePosY = 0;
	
	var ewalkloop = 0;
	var ecrouchloop = 0;
	var ejumploop = -1;
	
	var tempwl = 0;
	var tempcl = 0;
	var tempjl = 0;
	var eshootloop = 0;
	var egundrawloop = 0;
	var eshootStatus = false;
	var ermbDown = false;
	var x = 0;
	var y = 0;
	var ydif = 0;
	var ex = 0;
	var ey = 0;
	var eydif = 0;
	var gindex = 0;
	var musicOn = false;
	var ammoValue = 10;
	var eHealth = 1;
	var climbloop = 0;
	var upY = 0;
	
	var nOfEnemies = 0;
	var nOfBoxes = 0;
	var enemyArray = [];
	var boxArray = [];
	var enemy = [];
	var box = [];
	var killcount = 0;
	
	/* RESET GAME VARIABLES */
	function gamevars() {
	
		nOfEnemies = 0;
		nOfBoxes = 0;
		enemyArray = [];
		boxArray = [];
		enemy = [];
		box = [];
		shotArray = [];
		nOfShots = 0;
	
		upY = 0;
		climbloop = 0;
		aimCorrectA = 0;
		aimCorrectB = 0;
		aimCorrectC = 0;
		aimCorrectG = 0;
		aimCorrect = 0;	
	
		keyPressed = [null];
		walkloop = 0;
		crouchloop = 0;
		jumploop = -1;
		gundrawloop = 0;
		shootloop = 10;
		faceDirection = "left";
		standStatus = "stand";
		aimStatus = false;
		shootStatus = false;
		aimAngle = 0;
		mouseDistance = 0;
		rmbDown = false;
		zoomAmount = 0.7;
		groundY = 450;
		airtime = 0;
		aimTemp = false;

		bg = imagearray["map1"];
		bgX = Math.floor(Math.random() * (-2500)) + (-50);
		bgY = Math.floor(Math.random() * (-1500)) + (-50);
		difference = 0;
		newPosX = 0;
		oldPosX = 0;
		cameraX = 0;
		cameraY = 0;
		unfocus = false;
		focusValue = 10;
		focusDelta = 0;
	
	
		pCharX = c.width/2 - imagearray["invcharwalk"+walkloop].naturalWidth*zoomAmount/2;
		pCharY = 50;
		mousePosX = 0;
		mousePosY = 0;
		pHealth = 1;
	
		pDataArray = [];
		eDataArray = [];
		eCharX = -480;
		eCharY = -480;
		estandStatus = "stand";
		efaceDirection = "left";
		eaimStatus = false;
		emousePosX = 0;
		emousePosY = 0;
	
		ewalkloop = 0;
		ecrouchloop = 0;
		ejumploop = -1;
	
		tempwl = 0;
		tempcl = 0;
		tempjl = 0;
		eshootloop = 0;
		egundrawloop = 0;
		eshootStatus = false;
		ermbDown = false;
		x = 0;
		y = 0;
		ydif = 0;
		ex = 0;
		ey = 0;
		eydif = 0;
		gindex = 0;
		musicOn = false;
		if (pvpstatus) {
			ammoValue = 999;
		} else {
			ammoValue = 10;
		}
		eHealth = 1;
		gameclock = 1;
		frameclock = 1;
		killcount = 0;
	}
	
	/* KEY DOWN */
	function keyPressEvent(event) {
		event.preventDefault();
		if (!(keyPressed.includes(event.key))) {
			keyPressed.push(event.key);
			if (event.key === "s") {
				standStatus = "crouch";
			}
			else if ((event.key === " " && standStatus === "stand") && aimStatus === false) {
				aimtTemp = aimStatus;
				aimStatus = false;
				standStatus = "air";
				keyPressed.splice(keyPressed.indexOf(" "),1);
				keyPressed.push("jump");
				jumploop = 0;
			}
			else if ((event.key === "w" && standStatus === "stand") && aimStatus === false && upY < 175) {
				aimtTemp = aimStatus;
				aimStatus = false;
				standStatus = "climb";
				climbloop = 0;
			}
		}
		
	}
	
	/* KEY RELEASE */
	function keyUpEvent(event) {
		if (keyPressed.includes(event.key)) {
			keyPressed.splice(keyPressed.indexOf(event.key),1);
			if (event.key === "s" || event.key === "S") {
				keyPressed.push("ss");
			}
		}
	}

	/* RMB DOWN */
	function rightMouseDownEvent(event) {
		if (((event.which === 3 || event.button === 2) && gundrawloop === 0) && (standStatus === "stand" || standStatus === "crouch")) {
			var rect = c.getBoundingClientRect();
			mousePosX = (event.clientX-rect.left)/(rect.right-rect.left)*c.width + 6;
			mousePosY = (event.clientY-rect.top)/(rect.bottom-rect.top)*c.height + 6;
			aimStatus = true;

			rmbDown = true;
		}
	}
	
	/* MOUSE MOVEMENT */
	function mouseMoveEvent(event) {
		if (aimStatus) {
			var rect = c.getBoundingClientRect();
			mousePosX = (event.clientX-rect.left)/(rect.right-rect.left)*c.width + 6;
			mousePosY = (event.clientY-rect.top)/(rect.bottom-rect.top)*c.height + 6;
		}
	}
	
	/* RMB RELEASE */
	function rightMouseUpEvent(event) {
		if (event.which === 3 || event.button === 2) {
			gundrawloop = 0;
			rmbDown = false;
		}
	}
	
	/* LMB CLICK */
	function leftMouseClickEvent(event) {
		event.preventDefault();
		if (aimStatus === true && gameOn === true) {
			if (ammoValue > 0) {
				console.log("BANG");
				focusValue = focusValue + 100 - clientAcc*2;
				for (i = 0; i < 20; i = i + 1) {
					if (i === 19) {
						soundShot[0].currentTime = 0.2;
						soundShot[0].play();
					}
					if (soundShot[i].currentTime === 0) {
						soundShot[i].currentTime = 0.2;
						soundShot[i].play();
						break;
					}
				}
				shootStatus = true;
				shootloop = 0;
				ammoValue = ammoValue - 1;

			} else {
				
			}
		/* MUSIC / EXIT BUTTONS */
		} else {
			var rect = c.getBoundingClientRect();
			mousePosX = (event.clientX-rect.left)/(rect.right-rect.left)*c.width + 6;
			mousePosY = (event.clientY-rect.top)/(rect.bottom-rect.top)*c.height + 6;
			if ((mousePosX > 730 && mousePosX < 758 ) && (mousePosY > 454 && mousePosY < 475)) {
				if (musicOn) {
					bgmTrack.pause();
					bgmTrack.currentTime = 0;
					musicOn = false;
				} else {
					bgmTrack.play();
					musicOn = true;
				}
			}
			if ((mousePosX > 765 && mousePosX < 842 ) && (mousePosY > 453 && mousePosY < 476)) {
				gameOn = false;
				menustage = 1;
				gamevars();
				document.getElementById("menustage11").style.zIndex = "3";
				document.getElementById("menustage12").style.zIndex = "3";
				document.getElementById("backmain").style.zIndex = "3";
				document.getElementById("backstats").style.zIndex = "3";
			}
		}
	}
	
	
	var mapIndex = 0;
	var gameOn = false;
	var menustage = 1;
	var pvpstatus = false;
	var mygamename = "";
	var gamearraylength = 0;
	var distY = 0;
	var bloodloop = 0;
	var bloodSfx = false;
	var bloodX = 0;
	var bloodY = 0;
	var bloodInv = 0;
	var gameclock = 1;
	var frameclock = 0;
	var localgamearray = ["gamearray"];
	
	
	
	
	
	
	
	
	
	
	
	/*---------------*/
	
	/*  DRAWFUNCTION */
	
	/*---------------*/
	
	
	
	
	
	
	
	
	
	
	
	
	function redraw() {
		if (gameOn) {
			game(mapIndex, bg, pvpstatus);
		} else {
			menu();
		}
	}
	
	var looper = 0;
	var menuclock = 0;
	
	/* MENU */
	
	function menu() {
		
		/* CLEAR */
		c.width = c.width;
		
		/* BACKGROUND */
		bg = imagearray["menu"];
		context.drawImage(bg,0,0);
		looper = 0;
		$("#backmain").click(function(event) {
			event.preventDefault();
			menustage = 1;
			document.getElementById("singleplayermenu").style.zIndex = "-1";
			document.getElementById("multiplayermenu").style.zIndex = "-1";
			document.getElementById("menustage121").style.zIndex = "-1";
			document.getElementById("menustage122").style.zIndex = "-1";
			document.getElementById("gamenamediv").style.zIndex = "-1";
			document.getElementById("waitingroom").style.zIndex = "-1";
			document.getElementById("gameselect").style.zIndex = "-1";
			document.getElementById("losescreen").style.zIndex = "-1";
			document.getElementById("winscreen").style.zIndex = "-1";
			document.getElementById("levelupscreen").style.zIndex = "-1";
			document.getElementById("loader").style.zIndex = "-1";
			document.getElementById("menustage11").style.zIndex = "3";
			document.getElementById("menustage12").style.zIndex = "3";
			socket.emit("send message", ["deleteroom"]);
			
		});
		
		$("#backstats").click(function(event) {

			menustage = 4;
			document.getElementById("singleplayermenu").style.zIndex = "-1";
			document.getElementById("multiplayermenu").style.zIndex = "-1";
			document.getElementById("menustage121").style.zIndex = "-1";
			document.getElementById("menustage122").style.zIndex = "-1";
			document.getElementById("gamenamediv").style.zIndex = "-1";
			document.getElementById("waitingroom").style.zIndex = "-1";
			document.getElementById("gameselect").style.zIndex = "-1";
			document.getElementById("losescreen").style.zIndex = "-1";
			document.getElementById("winscreen").style.zIndex = "-1";
			document.getElementById("levelupscreen").style.zIndex = "3";
			document.getElementById("loader").style.zIndex = "-1";
			document.getElementById("menustage11").style.zIndex = "-1";
			document.getElementById("menustage12").style.zIndex = "-1";
			socket.emit("send message", ["deleteroom"]);
			
		});
		
		
		/* CHOOSE SINGLE/MULTI */
		if (menustage === 1) {
			if (menuclock === 0) {
				getHighscores();
			}
			menuclock = menuclock + 1;
			$("#menustage11").click(function(event) {
				event.preventDefault();
				menustage = 11;
				document.getElementById("menustage11").style.zIndex = "-1";
				document.getElementById("menustage12").style.zIndex = "-1";
				document.getElementById("singleplayermenu").style.zIndex = "3";
			});
			$("#menustage12").click(function(event) {
				event.preventDefault();
				menustage = 12;
				document.getElementById("menustage11").style.zIndex = "-1";
				document.getElementById("menustage12").style.zIndex = "-1";
				document.getElementById("menustage121").style.zIndex = "3";
				document.getElementById("menustage122").style.zIndex = "3";
			});
			
		/* CHOOSE MAP SINGLEPLAYER */
		} else if (menustage === 11) {
			$("#map1").click(function(event) {
				event.preventDefault();
				menustage = 1;
				document.getElementById("map1").style.zIndex = "-1";
				document.getElementById("singleplayermenu").style.zIndex = "-1";
				document.getElementById("backmain").style.zIndex = "-1";
				document.getElementById("backstats").style.zIndex = "-1";
				gameOn = true;
				mapIndex = 0;
				pvpstatus = false;
				bg = imagearray["map1"];
				gamevars();
				
			});
			
		/* MULTIPLAYER MENU */
		} else if (menustage === 12) {
			$("#menustage121").click(function(event) {
				event.preventDefault();
				menustage = 121;
				document.getElementById("menustage121").style.zIndex = "-1";
				document.getElementById("menustage122").style.zIndex = "-1";
				document.getElementById("multiplayermenu").style.zIndex = "3";
			});
			
			$("#menustage122").click(function(event) {
				event.preventDefault();
				menustage = 122;
				document.getElementById("menustage121").style.zIndex = "-1";
				document.getElementById("menustage122").style.zIndex = "-1";
				document.getElementById("gameselect").style.zIndex = "3";
			});
			
		/* CHOOSE MAP */
		} else if (menustage === 121) {
			looper = 0;
			$("#map12").click(function(event) {
				event.preventDefault();
				menustage = 1211;
				document.getElementById("multiplayermenu").style.zIndex = "-1";
				document.getElementById("gamenamediv").style.zIndex = "3";
				mapIndex = 0;
			});
			
		/* CREATE MULTIPLAYER */
		} else if (menustage === 1211) {
			
			if ($("#gamename").val().length > 8) {
				var tgn = $("#gamename").val();
				tgn = tgn.slice(0,8);
				$("#gamename").val(tgn);
			}
			$("#submitname").click(function(event) {
				if ($("#gamename").val() !== "") {
					event.preventDefault();
					menustage = 12111;
					console.log($("#gamename").val());
					clientGamename = $("#gamename").val();
					document.getElementById("gamenamediv").style.zIndex = "-1";
					document.getElementById("waitingroom").style.zIndex = "3";
					document.getElementById("loader").style.zIndex = "4";
					document.getElementById("loader").style.top = "300px";
					document.getElementById("loader").style.left = "370px";
					
					if (looper === 0) {
						pDataArray = ["gamename", clientUsername, clientGamename];
						socket.emit("send message", pDataArray);
						pDataArray = [];
					
						socket.emit("send message", ["getuserdata",clientUsername]);
						socket.on("userdata", function(userdata) {
							var t1 = JSON.stringify(userdata.msg);
							var user = t1.slice(t1.indexOf('username":')+11, t1.indexOf('password":')-3);
							if (clientUsername === user) {
								userdataparser(JSON.stringify(userdata.msg));
							}
						});
						looper = 1;
					}
				}
			});
		/* WAIT FOR OPPONENT */
		} else if (menustage === 12111) {
			socket.on("connectroom", function(data) {

				if (data.msg[1] === clientGamename) {
					document.getElementById("waitingroom").style.zIndex = "-1";
					document.getElementById("loader").style.zIndex = "-1";
					document.getElementById("backmain").style.zIndex = "-1";
					document.getElementById("backstats").style.zIndex = "-1";
					menustage = 1;
					gameOn = true;
					mapIndex = 0;
					pvpstatus = true;
					bg = imagearray["map1"];
					gamevars();
					
				}
			});
			
		/* JOIN MULTIPLAYER */	
		} else if (menustage === 122) {
			pDataArray = ["gamenamerequest"];
			socket.emit("send message", pDataArray);
			pDataArray = [];
			
			socket.on("new message", function(data) {
				if (data.msg[0] === "gamearray") {
					if (gamearraylength !== data.msg.length) {
						console.log(data.msg);
						gamearraylength = data.msg.length;
						document.getElementById("gamesel").innerHTML = "";
						for (i = 1; i < data.msg.length; i = i + 1 ) {
							document.getElementById("gamesel").innerHTML += "<option>"+data.msg[i]+"</option>";
						}
					}
				}
			});
			
			$("#pickgame").click(function() {
				if ($("#gamesel").val() !== "" && $("#gamesel").val() !== null) {
					clientGamename = $("#gamesel").val();
					socket.emit("send message", ["connectroom", clientGamename]);
					document.getElementById("gameselect").style.zIndex = "-1";
					document.getElementById("backmain").style.zIndex = "-1";
					document.getElementById("backstats").style.zIndex = "-1";
					menustage = 1;
					gameOn = true;
					mapIndex = 0;
					pvpstatus = true;
					bg = imagearray["map1"];
					gamevars();
					
				}
			});
			/* LOSESCREEN */
		} else if (menustage === 2) {
			/* WINSCREEN */
		} else if (menustage === 3) {
			/* LEVELUPSCREEN */
		} else if (menustage === 4)	{
			looper = 0;
			document.getElementById("addAgi").innerHTML = clientAgi;
			document.getElementById("addAcc").innerHTML = clientAcc;
			document.getElementById("addJump").innerHTML = clientJump;
			document.getElementById("pointsamount").innerHTML = "Assign points: "+clientPoints;
			$("#addAgi").click(function(event) {
				if (looper === 0) {
					if (clientPoints > 0) {
						clientAgi = clientAgi + 1;
						clientPoints = clientPoints - 1;
					}
					looper = 1;
				}
			});
			
			$("#addAcc").click(function(event) {
				if (looper === 0) {
					if (clientPoints > 0) {
						clientAcc = clientAcc + 1;
						clientPoints = clientPoints - 1;
					}
					looper = 1;
				}
			});
			
			$("#addJump").click(function(event) {
				if (looper === 0) {
					if (clientPoints > 0) {
						clientJump = clientJump + 1;
						clientPoints = clientPoints - 1;
					}
					looper = 1;
				}
			});
			
			$("#submitstats").click(function(event) {
				
				if (looper === 0) {
					socket.emit("send message", ["updatepoints", clientAgi, clientAcc, clientJump, clientPoints, clientUsername]);
					looper = 1;
				}
				document.getElementById("addAgi").innerHTML = clientAgi;
				document.getElementById("addAcc").innerHTML = clientAcc;
				document.getElementById("addJump").innerHTML = clientJump;
				document.getElementById("levelupscreen").style.zIndex = "-1";
				menustage = 1;
				document.getElementById("loader").style.zIndex = "-1";
				document.getElementById("menustage11").style.zIndex = "3";
				document.getElementById("menustage12").style.zIndex = "3";
				document.getElementById("clientAgi").innerHTML = "Agility: "+clientAgi;
				document.getElementById("clientAcc").innerHTML = "Accuracy: "+clientAcc;
				document.getElementById("clientJump").innerHTML = "Jump: "+clientJump;
				
				
			});
		}
		
	}
	
	/* GAME */
	
	function game(mapIndex, bg, pvpstatus) {

	
		/* CLEAR */
				
		c.width = c.width;
		
		/* GAME CLOCK */
		
		frameclock = frameclock + 1;
		if (frameclock === 30) {
			gameclock = gameclock + 1;
		}
		if (frameclock === 30) {
			frameclock = 1;
		}
		
		/* AIM CAMERA */
		
		if (gundrawloop === 0 && aimStatus === false) {
			cameraX = 0;
			cameraY = 0;
		} else if (gundrawloop === 11) {
			cameraX = - (mousePosX - c.width/2);
			cameraY = - (mousePosY - c.height/2);
		} else {
			if (rmbDown) {
				cameraX = - (mousePosX - c.width/2)*(gundrawloop/10);
				cameraY = - (mousePosY - c.height/2)*(gundrawloop/10);
			} else {
				cameraX = - (mousePosX - c.width/2)*((gundrawloop+2*(5-gundrawloop)+1)/10);
				cameraY = - (mousePosY - c.height/2)*((gundrawloop+2*(5-gundrawloop)+1)/10);
			}
		}

		/* TEMP VALUES */
		
		pCharX = c.width/2 - imagearray["invcharwalk"+walkloop].naturalWidth*zoomAmount/2;
		
		/* CLIMB CORRECTIONS */
		
		if (standStatus === "climb") {
			if (climbloop === 0) {	
				distY = groundY - upY;
				
				pCharY = pCharY - 100*zoomAmount;
			}
			if (climbloop > 9 && climbloop < 17) {
				pCharY = pCharY - (distY-330*zoomAmount)/7;
				bgY = bgY + distY/7;
				pCharY = pCharY + distY/7;
			}
			if (climbloop === 17) {
				pCharY = pCharY - 30*zoomAmount;
			}
			if (climbloop === 19) {
				pCharY = pCharY - 170*zoomAmount;
			}
			if (climbloop === 23) {
				pCharY = pCharY - 119*zoomAmount;
			}
		} else {
			pCharY = c.height - 320;
		}
		
		if (aimStatus) {
			if (faceDirection === "left") {
				pCharX = pCharX - 69 * zoomAmount;
			} else {
				pCharX = pCharX - 91 * zoomAmount;
			}
		}
		
		x = pCharX + cameraX;
		y = pCharY + cameraY;
		ydif = 0;
		
		/* GET UP Y */
		
		getUpY();
		
		/* GET GROUND Y */ 

		getGroundY();
		
		/* PGRAVITY */
		if (standStatus !== "climb") {
			if ((maps[mapIndex][2][gindex]-350*zoomAmount) - Math.abs(bgY-pCharY) > 0) {
				standStatus = "air";
				keyPressed.push("jump");
				if (jumploop === -1) {
					jumploop = 10;
				}
				airtime = airtime + 1/30;
				eydif = 0.5*200*airtime*airtime;
				if (eydif > Math.abs((maps[mapIndex][2][gindex]-350*zoomAmount) - Math.abs(bgY-pCharY))) {
					eydif = Math.abs((maps[mapIndex][2][gindex]-350*zoomAmount) - Math.abs(bgY-pCharY));				
				}
				bgY = bgY - eydif;			
			}
		
			if ((maps[mapIndex][2][gindex]-350*zoomAmount) - Math.abs(bgY-pCharY) <= 0) {
				airtime = 0;
				if (standStatus === "air") {
					jumploop = jumploop + 1;
				}
			}
		}


		
		/* BACKGROUND */
		
		context.drawImage(bg, bgX + cameraX, bgY + cameraY);
		
		/* CHECK IF HEALTH DROPS */
		
		var temphealth = pHealth;
		
		if (pvpstatus) {
			if (pHealth === 0 || pHealth < 0) {
				gameOn = false;
				menustage = 2;
				document.getElementById("backmain").style.zIndex = "3";
				document.getElementById("losescreen").style.zIndex = "3";
				document.getElementById("backstats").style.zIndex = "3";
			}
			if (eHealth === 0|| eHealth < 0) {
				gameOn = false;
				menustage = 3;
				document.getElementById("backmain").style.zIndex = "3";
				document.getElementById("winscreen").style.zIndex = "3";
				document.getElementById("backstats").style.zIndex = "3";
				menuclock = 0;
			}
		}
		
		
		/* PVP/PVE INFO */ 
		
		if (pvpstatus) {
			pvpdata();
		} else {
			pvedata();
		}
		
		if (temphealth !== pHealth) {
			for (i = 0; i < 20; i = i + 1) {
				if (i === 19) {
					soundGrunt[0].currentTime = 0;
					soundGrunt[0].play();
				}
				if (soundGrunt[i].currentTime === 0) {
					soundGrunt[i].play();
					break;
				}
			}
			if (pHealth === 0 || pHealth < 0) {
				gameOn = false;
				menustage = 2;
				document.getElementById("backmain").style.zIndex = "3";
				document.getElementById("losescreen").style.zIndex = "3";
				document.getElementById("backstats").style.zIndex = "3";
				if (pvpstatus === false) {
					clientXp = clientXp + killcount*10 * gameclock/10;
					var score = killcount*10 * gameclock/10
					var tempLevel = clientLevel;
					for (i = 0; i < xpCeilings.length; i = i + 1) {
						if (xpCeilings[i] > clientXp) {
							clientLevel = i-1;
							break;
						}
					}
					if (clientLevel > tempLevel) {
						clientPoints = clientPoints + 4*(clientLevel - tempLevel);
						menustage = 4;
						document.getElementById("losescreen").style.zIndex = "-1";
						document.getElementById("levelupscreen").style.zIndex = "3";
						document.getElementById("addAgi").innerHTML = clientAgi;
						document.getElementById("addAcc").innerHTML = clientAcc;
						document.getElementById("addJump").innerHTML = clientJump;
						document.getElementById("clientLevel").innerHTML = "Level: "+clientLevel;
						socket.emit("send message", ["updatelevel", clientLevel, clientPoints, clientUsername]);
						looper = 0;
					}
					document.getElementById("clientXp").innerHTML = "XP: "+clientXp;
					socket.emit("send message", ["updatexp", clientXp, clientUsername, score]);
					menuclock = 0;
				}
				
			}
			if (eHealth === 0 || eHealth < 0) {
				gameOn = false;
				menustage = 3;
				document.getElementById("backmain").style.zIndex = "3";
				document.getElementById("winscreen").style.zIndex = "3";
				document.getElementById("backstats").style.zIndex = "3";
				menuclock = 0;
			}
		}
		
		/* CHECK IF SHOTS HIT */ 
		
		if (shootloop === 0) {
			didItHit();
		}
		
		
		
		/* UNFOCUS SYSTEM */
		
		if (rmbDown) {
			unfocus = true;
		} else {
			unfocus = false;
		}
		if (unfocus) {
			
			mouseDistance = Math.sqrt((mousePosY - (y +(75+14)*zoomAmount))*(mousePosY - (y +(75+14)*zoomAmount))+(mousePosX - (x + (185-7)*zoomAmount))*(mousePosX - (x + (185-7)*zoomAmount)));
			
			
			if (crouchloop > 0 && crouchloop < 9) {
				focusValue = focusValue + 20 -clientAcc*0.2;
			} else if ((keyPressed.includes("a") || keyPressed.includes("d")) && standStatus === "stand" ) {
				focusValue = focusValue + 5 - clientAcc*0.02;
			} 
			
			focusValue = focusValue - focusValue / 10;
			
			if (focusValue < 5) {
				focusValue = 5;
			} else if (focusValue > 300 - clientAcc) {
				focusValue = 300 - clientAcc;
			}

			aimAngle = (Math.atan((mousePosY - (y + (75+14)*zoomAmount))/(mousePosX - (x + (185-7)*zoomAmount))))
			focusDelta = ((180*focusValue)/(Math.PI*mouseDistance))*(Math.PI/180);
			var cursorFocusPosX = mouseDistance*Math.cos((aimAngle + focusDelta));
			var cursorFocusPosY = mouseDistance*Math.sin((aimAngle + focusDelta));
			var cursorFocusNegX = mouseDistance*Math.cos((aimAngle - focusDelta));
			var cursorFocusNegY = mouseDistance*Math.sin((aimAngle - focusDelta));
			if (faceDirection === "right") {
				context.drawImage(imagearray["chcursor"], (x + (185-7)*zoomAmount) + cursorFocusPosX - 6, (y +(75+14)*zoomAmount) + cursorFocusPosY - 6);
				context.drawImage(imagearray["chcursor"], (x + (185-7)*zoomAmount) + cursorFocusNegX - 6, (y +(75+14)*zoomAmount) + cursorFocusNegY - 6);
			} else {
				context.drawImage(imagearray["chcursor"], (x + (185-7)*zoomAmount) - cursorFocusPosX - 6, (y +(75+14)*zoomAmount) - cursorFocusPosY - 6);
				context.drawImage(imagearray["chcursor"], (x + (185-7)*zoomAmount) - cursorFocusNegX - 6, (y +(75+14)*zoomAmount) - cursorFocusNegY - 6);

			}
			
		}
		

		
		/* SHOOTING SOUNDS */
		
		for (i = 0; i < 20; i = i + 1) {
			if (soundShot[i].currentTime > 0.6) {
				soundShot[i].currentTime = 0;
				soundShot[i].pause();
				for (f = 0; f < 20; f = f + 1) {
					if (f === 19) {
						soundShell[0].currentTime = 0;
						soundShell[0].play();
					}
					if (soundShell[f].currentTime === 0) {
						soundShell[f].play();
						break;
					}
				}
			}
		}
		
		
		
		/* MOVING */
		
		
		if (standStatus === "stand") {
			
			/* AIMING */
			
			if (aimStatus) {
				if (mousePosX < x + 190 * zoomAmount) {
					faceDirection = "left";
				} else {
					faceDirection = "right";
				}
				if (gundrawloop < 11) {
					shootStatus = false;
					if (rmbDown) {
						if (faceDirection === "left") {
							context.drawImage(imagearray["armlessstand"] ,x ,y , imagearray["armlessstand"].naturalWidth * zoomAmount, imagearray["armlessstand"].naturalHeight * zoomAmount)
							context.drawImage(imagearray["gunarms"+gundrawloop],x,y , imagearray["gunarms"+gundrawloop].naturalWidth * zoomAmount, imagearray["gunarms"+gundrawloop].naturalHeight * zoomAmount);
						} else {
							context.drawImage(imagearray["invarmlessstand"],x,y , imagearray["invarmlessstand"].naturalWidth * zoomAmount, imagearray["invarmlessstand"].naturalHeight * zoomAmount)
							context.drawImage(imagearray["invgunarms"+gundrawloop],x,y , imagearray["invgunarms"+gundrawloop].naturalWidth * zoomAmount, imagearray["invgunarms"+gundrawloop].naturalHeight * zoomAmount);
						}
						gundrawloop = gundrawloop + 1;
					} else {
						if (faceDirection === "left") {
							context.drawImage(imagearray["armlessstand"],x,y , imagearray["armlessstand"].naturalWidth * zoomAmount, imagearray["armlessstand"].naturalHeight * zoomAmount)
							context.drawImage(imagearray["gunarmsreverse"+gundrawloop],x,y , imagearray["gunarmsreverse"+gundrawloop].naturalWidth * zoomAmount, imagearray["gunarmsreverse"+gundrawloop].naturalHeight * zoomAmount);
						} else {
							context.drawImage(imagearray["invarmlessstand"],x,y , imagearray["invarmlessstand"].naturalWidth * zoomAmount, imagearray["invarmlessstand"].naturalHeight * zoomAmount)
							context.drawImage(imagearray["invgunarmsreverse"+gundrawloop],x,y , imagearray["invgunarmsreverse"+gundrawloop].naturalWidth * zoomAmount, imagearray["invgunarmsreverse"+gundrawloop].naturalHeight * zoomAmount);
						}
						gundrawloop = gundrawloop + 1;
						if (gundrawloop > 10) {
							aimStatus = false;
							gundrawloop = 0;
							if (faceDirection === "left") {
								pCharX = pCharX + 69 * zoomAmount;
							} else {
								pCharX = pCharX + 91 * zoomAmount;
							}
						}
					}
				} else {
					
					/* MOVING WHILE AIMING */
					if (keyPressed.includes("d")) {
						if (faceDirection === "right") {
							context.drawImage(imagearray["invarmlesscharwalk"+walkloop],x,y , imagearray["invarmlesscharwalk"+walkloop].naturalWidth * zoomAmount, imagearray["invarmlesscharwalk"+walkloop].naturalHeight * zoomAmount);
							walkloop = walkloop + 1;
						} else {
							context.drawImage(imagearray["armlesscharwalk"+walkloop],x,y , imagearray["armlesscharwalk"+walkloop].naturalWidth * zoomAmount, imagearray["armlesscharwalk"+walkloop].naturalHeight * zoomAmount);
							walkloop = walkloop - 1;
						}
						if (walkloop < 0) {
							walkloop = 9;
						}
						if (walkloop > 9) {
							walkloop = 0;
						}
						/* BG SCROLL */
						bgX = bgX - (6 + clientAgi*0.1) * zoomAmount;
						if(bgX < - bg.naturalWidth + c.width/2 + imagearray["invcharwalk"+walkloop].naturalWidth * zoomAmount/2) {
							bgX = - bg.naturalWidth + c.width/2 + imagearray["invcharwalk"+walkloop].naturalWidth * zoomAmount/2;
						}
						
					} else if (keyPressed.includes("a")) {
						if (faceDirection === "left") {
							context.drawImage(imagearray["armlesscharwalk"+walkloop],x,y , imagearray["armlesscharwalk"+walkloop].naturalWidth * zoomAmount, imagearray["armlesscharwalk"+walkloop].naturalHeight * zoomAmount);
							walkloop = walkloop + 1;
						} else {
							context.drawImage(imagearray["invarmlesscharwalk"+walkloop],x,y , imagearray["invarmlesscharwalk"+walkloop].naturalWidth * zoomAmount, imagearray["invarmlesscharwalk"+walkloop].naturalHeight * zoomAmount);
							walkloop = walkloop - 1;
						}
						if (walkloop < 0) {
							walkloop = 9;
						}
						if (walkloop > 9) {
							walkloop = 0;
						}
						/* BG SCROLL */
						bgX = bgX +  (6 + clientAgi*0.1)  * zoomAmount;
						if(bgX > c.width/2 - imagearray["invcharwalk"+walkloop].naturalWidth * zoomAmount / 2) {
							bgX = c.width/2 - imagearray["invcharwalk"+walkloop].naturalWidth * zoomAmount / 2;
						}
						
					} else {
						if (faceDirection === "left") {
							context.drawImage(imagearray["armlessstand"],x,y , imagearray["armlessstand"].naturalWidth * zoomAmount, imagearray["armlessstand"].naturalHeight * zoomAmount);
						} else if (faceDirection === "right") {
							context.drawImage(imagearray["invarmlessstand"],x,y , imagearray["invarmlessstand"].naturalWidth * zoomAmount, imagearray["invarmlessstand"].naturalHeight * zoomAmount);
						}
					}
					
					/* ARMANIMATION */
					
					if (faceDirection === "left") {
						/* MOUSE DIST FROM CHAR(75+14 ja 185-7 = SHOULDERCOORDINATES)*/
						mouseDistance = Math.sqrt((mousePosY - (y +(75+14)*zoomAmount))*(mousePosY - (y +(75+14)*zoomAmount))+(mousePosX - (x + (185-7)*zoomAmount))*(mousePosX - (x + (185-7)*zoomAmount)));
						/* FIX ROTATION ERROR */
						
						aimCorrectA = 70 * zoomAmount;
						aimCorrectB = mouseDistance - Math.sqrt((233 * zoomAmount)*(233 * zoomAmount) + (aimCorrectA)*(aimCorrectA));
						aimCorrectG = 180 - Math.atan((aimCorrectA)/(233*zoomAmount))*180/Math.PI;
						aimCorrectC = Math.sqrt(aimCorrectA*aimCorrectA + aimCorrectB*aimCorrectB - 2*aimCorrectA*aimCorrectB*Math.cos(aimCorrectG));
						aimCorrect = (aimCorrectB*aimCorrectB + aimCorrectC*aimCorrectC - aimCorrectA*aimCorrectA)/(2*aimCorrectB*aimCorrectC);
						
						/* AIM ANGLE */
						aimAngle = (Math.atan((mousePosY - (y + (75+14)*zoomAmount))/(mousePosX - (x + (185-7)*zoomAmount))))+aimCorrect*Math.PI/180;
						
						context.save();
						/* MOVE TO WHERE SHOULDERS CONNECT */
						context.translate(x+(185-7)*zoomAmount, y+(75+14)*zoomAmount);
						context.rotate(aimAngle);
						
						/* SHOOTLOOP */
						
						if (shootloop > 9) {
							shootStatus = false;
						}
						if (shootStatus) {
							context.drawImage(imagearray["pistolshoot"+shootloop],0-233*zoomAmount,0-88*zoomAmount , imagearray["pistolshoot"+shootloop].naturalWidth * zoomAmount, imagearray["pistolshoot"+shootloop].naturalHeight * zoomAmount)
							shootloop = shootloop + 1;
						} else {
							context.drawImage(imagearray["gunarm"],0-233*zoomAmount,0-88*zoomAmount , imagearray["gunarm"].naturalWidth * zoomAmount, imagearray["gunarm"].naturalHeight * zoomAmount);
						}
						
						context.restore();
					} else {
						/* MOUSE DIST FROM CHAR (75-16 ja 185+14 = SHOULDERCOORDINATES)*/
						mouseDistance = Math.sqrt((mousePosY - (y +(75+14)*zoomAmount))*(mousePosY - (y +(75+14)*zoomAmount))+(mousePosX - (x + (185+14)*zoomAmount))*(mousePosX - (x + (185+14)*zoomAmount)));
						/* FIX ROTATION ERROR*/
						
						aimCorrectA = 70 * zoomAmount;
						aimCorrectB = mouseDistance - Math.sqrt((233 * zoomAmount)*(233 * zoomAmount) + (aimCorrectA)*(aimCorrectA));
						aimCorrectG = 180 - Math.atan((aimCorrectA)/(233*zoomAmount))*180/Math.PI;
						aimCorrectC = Math.sqrt(aimCorrectA*aimCorrectA + aimCorrectB*aimCorrectB - 2*aimCorrectA*aimCorrectB*Math.cos(aimCorrectG));
						aimCorrect = (aimCorrectB*aimCorrectB + aimCorrectC*aimCorrectC - aimCorrectA*aimCorrectA)/(2*aimCorrectB*aimCorrectC);
						
						/* AIMANGLE */
						aimAngle = (Math.atan((mousePosY - (y + (75+14)*zoomAmount))/(mousePosX - (x + (185+14)*zoomAmount))))-aimCorrect*Math.PI/180;
					
						context.save();
						/* MOVE TO WHERE SHOULDERS CONNECT */
						context.translate(x+(185+16)*zoomAmount, y+(75+14)*zoomAmount);
						context.rotate(aimAngle);
						
						/* SHOOTLOOP */
						
						if (shootloop > 9) {
							shootStatus = false;
						}
						if (shootStatus) {
							context.drawImage(imagearray["invpistolshoot"+shootloop],0-27*zoomAmount,0-88*zoomAmount , imagearray["invpistolshoot"+shootloop].naturalWidth * zoomAmount, imagearray["invpistolshoot"+shootloop].naturalHeight * zoomAmount)
							shootloop = shootloop + 1;
						} else {
							context.drawImage(imagearray["invgunarm"],0-27*zoomAmount,0-88*zoomAmount , imagearray["invgunarm"].naturalWidth * zoomAmount, imagearray["invgunarm"].naturalHeight * zoomAmount);
						}
						context.restore();
					}
				}
				
			/* NOT AIMING */
			
			} else {
				if (keyPressed.includes("d")) {
					context.drawImage(imagearray["invcharwalk"+walkloop],x,y , imagearray["invcharwalk"+walkloop].naturalWidth * zoomAmount, imagearray["invcharwalk"+walkloop].naturalHeight * zoomAmount);
					walkloop = walkloop + 1;
					if (walkloop > 9) {
						walkloop = 0;
					}
					/* BG SCROLLING */
					bgX = bgX - (15 + clientAgi*0.2)  * zoomAmount;
					if (bgX < -bg.naturalWidth + c.width/2 + imagearray["invcharwalk"+walkloop].naturalWidth*zoomAmount/2) {
						bgX = -bg.naturalWidth + c.width/2 + imagearray["invcharwalk"+walkloop].naturalWidth*zoomAmount/2;
					}
					
					faceDirection = "right";
					
				} else if (keyPressed.includes("a")) {
					context.drawImage(imagearray["charwalk"+walkloop],x,y , imagearray["charwalk"+walkloop].naturalWidth * zoomAmount, imagearray["charwalk"+walkloop].naturalHeight * zoomAmount);
					walkloop = walkloop + 1;
					if (walkloop > 9) {
						walkloop = 0;
					}
					/* BG SCROLLING */ 
					bgX = bgX + (15 + clientAgi*0.2)  * zoomAmount;
					if (bgX > c.width/2 - imagearray["invcharwalk"+walkloop].naturalWidth * zoomAmount / 2) {
						bgX = c.width/2 - imagearray["invcharwalk"+walkloop].naturalWidth * zoomAmount / 2;
					}
					
					faceDirection = "left";
					
				} else {
					if (faceDirection === "left") {
						context.drawImage(imagearray["charstand1"],x,y , imagearray["charstand1"].naturalWidth * zoomAmount, imagearray["charstand1"].naturalHeight * zoomAmount);
					} else if (faceDirection === "right") {
						context.drawImage(imagearray["invcharstand1"],x,y , imagearray["invcharstand1"].naturalWidth * zoomAmount, imagearray["invcharstand1"].naturalHeight * zoomAmount);
					}
				}
			}
		}
		
		/* CROUCHING */
		
		 else if (standStatus === "crouch") {
			if (aimStatus) {
				if (mousePosX < x + 190 * zoomAmount) {
					faceDirection = "left";
				} else {
					faceDirection = "right";
				}
				if (gundrawloop < 11) {
					if (rmbDown) {
						if (faceDirection === "left") {
							context.drawImage(imagearray["armlesscharcrouch"+crouchloop],x,y , imagearray["armlesscharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["armlesscharcrouch"+crouchloop].naturalHeight * zoomAmount)
							context.drawImage(imagearray["gunarms"+gundrawloop],x + midcrouchingX[crouchloop] * zoomAmount ,y + midcrouchingY[crouchloop] * zoomAmount , imagearray["gunarms"+gundrawloop].naturalWidth * zoomAmount, imagearray["gunarms"+gundrawloop].naturalHeight * zoomAmount);
						} else {
							context.drawImage(imagearray["invarmlesscharcrouch"+crouchloop],x,y , imagearray["invarmlesscharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["invarmlesscharcrouch"+crouchloop].naturalHeight * zoomAmount)
							context.drawImage(imagearray["invgunarms"+gundrawloop],x + invmidcrouchingX[crouchloop] * zoomAmount ,y + invmidcrouchingY[crouchloop] * zoomAmount , imagearray["invgunarms"+gundrawloop].naturalWidth * zoomAmount, imagearray["invgunarms"+gundrawloop].naturalHeight * zoomAmount);
						}
						gundrawloop = gundrawloop + 1;
					} else {
						if (faceDirection === "left") {
							context.drawImage(imagearray["armlesscharcrouch"+crouchloop],x,y , imagearray["armlesscharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["armlesscharcrouch"+crouchloop].naturalHeight * zoomAmount)
							context.drawImage(imagearray["gunarmsreverse"+gundrawloop],x + midcrouchingX[crouchloop] * zoomAmount ,y + midcrouchingY[crouchloop] * zoomAmount , imagearray["gunarmsreverse"+gundrawloop].naturalWidth * zoomAmount, imagearray["gunarmsreverse"+gundrawloop].naturalHeight * zoomAmount);
						} else {
							context.drawImage(imagearray["invarmlesscharcrouch"+crouchloop],x,y , imagearray["invarmlesscharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["invarmlesscharcrouch"+crouchloop].naturalHeight * zoomAmount)
							context.drawImage(imagearray["invgunarmsreverse"+gundrawloop],x + invmidcrouchingX[crouchloop] * zoomAmount ,y + invmidcrouchingY[crouchloop] * zoomAmount , imagearray["invgunarmsreverse"+gundrawloop].naturalWidth * zoomAmount, imagearray["invgunarmsreverse"+gundrawloop].naturalHeight * zoomAmount);
						}
						gundrawloop = gundrawloop + 1;
						if (gundrawloop > 10) {
							aimStatus = false;
							gundrawloop = 0;
							if (faceDirection === "left") {
								pCharX = pCharX + 69 * zoomAmount;
							} else {
								pCharX = pCharX + 91 * zoomAmount;
							}
						}
					}
				} else {
					
					/* CROUCHING ANIMATION */
					
					if (keyPressed.includes("s")) {
						if (faceDirection === "left") {
							context.drawImage(imagearray["armlesscharcrouch"+crouchloop],x,y , imagearray["armlesscharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["armlesscharcrouch"+crouchloop].naturalHeight * zoomAmount);
						} else {
							context.drawImage(imagearray["invarmlesscharcrouch"+crouchloop],x,y , imagearray["invarmlesscharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["invarmlesscharcrouch"+crouchloop].naturalHeight * zoomAmount);
						}
						if (crouchloop < 9) {
							crouchloop = crouchloop + 1;
						}
						standStatus = "crouch";
					} else if (keyPressed.includes("ss")) {
						if (faceDirection === "left") {
							context.drawImage(imagearray["armlesscharcrouch"+crouchloop],x,y , imagearray["armlesscharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["armlesscharcrouch"+crouchloop].naturalHeight * zoomAmount);
						} else {
							context.drawImage(imagearray["invarmlesscharcrouch"+crouchloop],x,y , imagearray["invarmlesscharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["invarmlesscharcrouch"+crouchloop].naturalHeight * zoomAmount);
						}
						if (crouchloop > 1) {
							crouchloop = crouchloop - 1;
						} else {
							standStatus = "stand";
							keyPressed.splice(keyPressed.indexOf("ss",1));
							crouchloop = 0;
						}
					}
					
					/* ARMANIMATION */
					
					if (faceDirection === "left") {
						/* MOUSE DIST FROM CHAR (75-16 ja 185+14 = SHOULDERCOORDINATES)*/
						mouseDistance = Math.sqrt((mousePosY - (y + crouchingY[crouchloop]*zoomAmount))*(mousePosY - (y + crouchingY[crouchloop]*zoomAmount))+(mousePosX - (x + crouchingX[crouchloop]*zoomAmount))*(mousePosX - (x + crouchingX[crouchloop]*zoomAmount)));
						/* FIX ROTATION ERROR */
						
						aimCorrectA = 70 * zoomAmount;
						aimCorrectB = mouseDistance - Math.sqrt((233 * zoomAmount)*(233 * zoomAmount) + (aimCorrectA)*(aimCorrectA));
						aimCorrectG = 180 - Math.atan((aimCorrectA)/(233*zoomAmount))*180/Math.PI;
						aimCorrectC = Math.sqrt(aimCorrectA*aimCorrectA + aimCorrectB*aimCorrectB - 2*aimCorrectA*aimCorrectB*Math.cos(aimCorrectG));
						aimCorrect = (aimCorrectB*aimCorrectB + aimCorrectC*aimCorrectC - aimCorrectA*aimCorrectA)/(2*aimCorrectB*aimCorrectC);
						
						/* AIMANGLE */
						aimAngle = (Math.atan((mousePosY - (y + crouchingY[crouchloop]*zoomAmount))/(mousePosX - (x + crouchingX[crouchloop]*zoomAmount))))+aimCorrect*Math.PI/180;

						
						context.save();
						/* MOVE TO WHERE SHOULDERS CONNECT */
						context.translate(x + crouchingX[crouchloop]* zoomAmount, y + crouchingY[crouchloop]* zoomAmount);
						context.rotate(aimAngle);

						/* SHOOTLOOP */
						
						if (shootloop > 9) {
							shootStatus = false;
						}
						if (shootStatus) {
							context.drawImage(imagearray["pistolshoot"+shootloop],0-233* zoomAmount,0-88* zoomAmount , imagearray["pistolshoot"+shootloop].naturalWidth * zoomAmount, imagearray["pistolshoot"+shootloop].naturalHeight * zoomAmount)
							shootloop = shootloop + 1;
						} else {
							context.drawImage(imagearray["gunarm"],0-233* zoomAmount,0-88* zoomAmount , imagearray["gunarm"].naturalWidth * zoomAmount, imagearray["gunarm"].naturalHeight * zoomAmount);
						}
						
						context.restore();
						
					} else {
						/* MOUSE DIST FROM CHAR (75-16 ja 185+14 = SHOULDERCOORDINATES)*/
						mouseDistance = Math.sqrt((mousePosY - (y + invcrouchingY[crouchloop]*zoomAmount))*(mousePosY - (y + invcrouchingY[crouchloop]*zoomAmount))+(mousePosX - (x + invcrouchingX[crouchloop]*zoomAmount))*(mousePosX - (x + invcrouchingX[crouchloop]*zoomAmount)));
						
						/* FIX ROTATION ERROR */
						
						aimCorrectA = 70 * zoomAmount;
						aimCorrectB = mouseDistance - Math.sqrt((233 * zoomAmount)*(233 * zoomAmount) + (aimCorrectA)*(aimCorrectA));
						aimCorrectG = 180 - Math.atan((aimCorrectA)/(233*zoomAmount))*180/Math.PI;
						aimCorrectC = Math.sqrt(aimCorrectA*aimCorrectA + aimCorrectB*aimCorrectB - 2*aimCorrectA*aimCorrectB*Math.cos(aimCorrectG));
						aimCorrect = (aimCorrectB*aimCorrectB + aimCorrectC*aimCorrectC - aimCorrectA*aimCorrectA)/(2*aimCorrectB*aimCorrectC);
						
						/* AIMANGLE */
						aimAngle = (Math.atan((mousePosY - (y + invcrouchingY[crouchloop]*zoomAmount))/(mousePosX - (x + invcrouchingX[crouchloop]*zoomAmount))))-aimCorrect*Math.PI/180;
					
						context.save();
						/* MOVE TO THE POINT WHERE SHOULDERS CONNECT */
						context.translate(x + invcrouchingX[crouchloop]* zoomAmount, y + invcrouchingY[crouchloop]* zoomAmount);
						context.rotate(aimAngle);
						
						/* SHOOT LOOP */
						
						if (shootloop > 9) {
							shootStatus = false;
						}
						if (shootStatus) {
							context.drawImage(imagearray["invpistolshoot"+shootloop],0-27* zoomAmount,0-88* zoomAmount , imagearray["invpistolshoot"+shootloop].naturalWidth * zoomAmount, imagearray["invpistolshoot"+shootloop].naturalHeight * zoomAmount)
							shootloop = shootloop + 1;
						} else {
							context.drawImage(imagearray["invgunarm"],0-27* zoomAmount,0-88* zoomAmount , imagearray["invgunarm"].naturalWidth * zoomAmount, imagearray["invgunarm"].naturalHeight * zoomAmount);
						}
						
						context.restore();
					}
				}
			} else {
				
				/* NOT AIMING */
				
				if (keyPressed.includes("s")) {
					if (faceDirection === "left") {
						context.drawImage(imagearray["charcrouch"+crouchloop],x,y , imagearray["charcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["charcrouch"+crouchloop].naturalHeight * zoomAmount);
					} else {
						context.drawImage(imagearray["invcharcrouch"+crouchloop],x,y , imagearray["invcharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["invcharcrouch"+crouchloop].naturalHeight * zoomAmount);
					}
					if (crouchloop < 9) {
						crouchloop = crouchloop + 1;
					}
					standStatus = "crouch";
				} else if (keyPressed.includes("ss")) {
					if (faceDirection === "left") {
						context.drawImage(imagearray["charcrouch"+crouchloop],x,y , imagearray["charcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["charcrouch"+crouchloop].naturalHeight * zoomAmount);
					} else {
						context.drawImage(imagearray["invcharcrouch"+crouchloop],x,y , imagearray["invcharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["invcharcrouch"+crouchloop].naturalHeight * zoomAmount);
					}
					if (crouchloop > 1) {
						crouchloop = crouchloop - 1;
					} else {
						standStatus = "stand";
						keyPressed.splice(keyPressed.indexOf("ss",1));
						crouchloop = 0;
					}
				}
			}
			
		/* IN AIR */
		
		} else if (standStatus === "air") {
			
			/* JUMP */
			
			if (keyPressed.includes("jump")) {
				
				if (jumploop > -1 && jumploop < 5){
					
					bgscroll();

					if (faceDirection === "left") {
						context.drawImage(imagearray["jump"+jumploop],x,y , imagearray["jump"+jumploop].naturalWidth * zoomAmount, imagearray["jump"+jumploop].naturalHeight * zoomAmount);
					} else {
						context.drawImage(imagearray["invjump"+jumploop],x,y, imagearray["invjump"+jumploop].naturalWidth * zoomAmount, imagearray["invjump"+jumploop].naturalHeight * zoomAmount);
					}
					jumploop = jumploop + 1;
				
				} else if (jumploop > 4 && jumploop < 10) {
					
					ydif = ((50+clientJump*0.2)-2*jumploop)*zoomAmount;
					bgY = bgY+ydif;
					y = pCharY;
					
					bgscroll();
					
					if (faceDirection === "left") {
						context.drawImage(imagearray["jump"+jumploop],x,y , imagearray["jump"+jumploop].naturalWidth * zoomAmount, imagearray["jump"+jumploop].naturalHeight * zoomAmount);
					} else {
						context.drawImage(imagearray["invjump"+jumploop],x,y, imagearray["invjump"+jumploop].naturalWidth * zoomAmount, imagearray["invjump"+jumploop].naturalHeight * zoomAmount);
					}
					jumploop = jumploop + 1;
					
				} else if (jumploop > 10 && jumploop < 14){

					bgscroll();
					
					if (faceDirection === "left") {
						context.drawImage(imagearray["jump"+jumploop],x,y , imagearray["jump"+jumploop].naturalWidth * zoomAmount, imagearray["jump"+jumploop].naturalHeight * zoomAmount);
					} else {
						context.drawImage(imagearray["invjump"+jumploop],x,y, imagearray["invjump"+jumploop].naturalWidth * zoomAmount, imagearray["invjump"+jumploop].naturalHeight * zoomAmount);
					}
					
				} else if (jumploop === 10){
					
					bgscroll();
					
					if (faceDirection === "left") {
						context.drawImage(imagearray["jump"+10],x,y , imagearray["jump"+8].naturalWidth * zoomAmount, imagearray["jump"+10].naturalHeight * zoomAmount);
					} else {
						context.drawImage(imagearray["invjump"+10],x,y , imagearray["invjump"+8].naturalWidth * zoomAmount, imagearray["invjump"+10].naturalHeight * zoomAmount);
					}
					
				} else if (jumploop > 13) {
					
					bgscroll();
					
					
					if (faceDirection === "left") {
						context.drawImage(imagearray["jump"+jumploop],x,y , imagearray["jump"+jumploop].naturalWidth * zoomAmount, imagearray["jump"+jumploop].naturalHeight * zoomAmount);
					} else {
						context.drawImage(imagearray["invjump"+jumploop],x,y, imagearray["invjump"+jumploop].naturalWidth * zoomAmount, imagearray["invjump"+jumploop].naturalHeight * zoomAmount);
					}
					
					aimStatus = aimTemp;
					standStatus = "stand";
					keyPressed.splice(keyPressed.indexOf("jump",1));
					jumploop = -1;
				}
				
				
			/* FALL */
			
			} else {
				
				bgscroll();
				
				if (faceDirection === "left") {
					context.drawImage(imagearray["jump"+10],x,y , imagearray["jump"+8].naturalWidth * zoomAmount, imagearray["jump"+10].naturalHeight * zoomAmount);
				} else {
					context.drawImage(imagearray["invjump"+10],x,y , imagearray["invjump"+8].naturalWidth * zoomAmount, imagearray["invjump"+10].naturalHeight * zoomAmount);
				}
			}
			
		/* CLIMB */
		} else if (standStatus === "climb") {
			if (faceDirection === "left") {
				context.drawImage(imagearray["jumpclimbtest"+climbloop], x, y, imagearray["jumpclimbtest"+climbloop].naturalWidth * zoomAmount, imagearray["jumpclimbtest"+climbloop].naturalHeight * zoomAmount);
			} else {
				context.drawImage(imagearray["invjumpclimbtest"+climbloop], x, y, imagearray["invjumpclimbtest"+climbloop].naturalWidth * zoomAmount, imagearray["invjumpclimbtest"+climbloop].naturalHeight * zoomAmount);
			}
			climbloop = climbloop + 1;
			if (climbloop > 27) {
				standStatus = "stand";
				/*bgY = bgY + distY;*/
				pCharY = c.height - 320;
				getGroundY();
			}
	
		} else {
			if (faceDirection === "left") {
				context.drawImage(imagearray["charstand1"],x,y);
			} else if (faceDirection === "right") {
				context.drawImage(imagearray["invcharstand1"],x,y);
			}
			
		}
		
		
		
		
		/* FUNCTIONS*/
		
		
		

		drawHUD();
		
		/* HUD */
		
		function drawHUD() {
			
			context.fillStyle="#d3d3d3";
			context.fillRect(131, 450, 258, 26);
			context.fillStyle="#ff0000";
			context.fillRect(131, 450, 258*pHealth, 26);
			
			context.drawImage(imagearray["HUD"], 0, 0);
			
			if (!musicOn) {
				context.beginPath();
				context.moveTo(735, 457);
				context.lineTo(754, 473);
				context.stroke();
			}
			
			context.font="bold 22px Arial";
			context.fillStyle="#000000";
			context.fillText(ammoValue, 495, 473);
			context.font="bold 22px Arial";
			context.fillStyle="#000000";
			context.fillText(ammoValue, 497, 473);
			context.font="19px Arial";
			context.fillStyle="#FFFF00";
			context.fillText(ammoValue, 496, 472);
		}
		
		/* HIT DETECT */
		
		function didItHit() {
			if (pvpstatus) {
				var shotAngle = aimAngle*180/Math.PI + focusDelta*180/Math.PI*Math.random()*(1-(-1))+(-1);
				var k = Math.tan(shotAngle*Math.PI/180);
				if (standStatus === "crouch") {
					var y1 = (y + crouchingY[crouchloop]*zoomAmount);
					var x1 = (x + crouchingX[crouchloop]*zoomAmount);
				} else {
					var y1 = (y + (75+14)*zoomAmount);
					var x1 = (x + (185-7)*zoomAmount);
				}
				if (eaimStatus) {
					var tempx = imagearray["armlessstand"].naturalWidth*zoomAmount/2;
				} else {
					var tempx = imagearray["charwalk"+2].naturalWidth*zoomAmount/2;
				}

				var yIn = k*(bgX-500 - x1) + y1;
				var yOut = k*(bgX + 3500 - x1) + y1;
				if (faceDirection === "left") {
					context.beginPath();
					context.moveTo(x1,y1);
					context.lineTo(bgX-500,yIn);
					context.strokeStyle = '#ffff00';
					context.stroke();
				} else {
					context.beginPath();
					context.moveTo(x1,y1);
					context.lineTo(bgX+3500,yOut);
					context.strokeStyle = '#ffff00';
					context.stroke();
				}
				
				yIn = k*(eCharX + tempx - x1) + y1;
				
				context.beginPath();
				context.moveTo(x1,y1);
				context.lineTo(eCharX+tempx,yIn);
				context.strokeStyle = '#ffff00';
				context.stroke();

			
				if (estandStatus === "crouch") {
					if (yIn > eCharY+70 && yIn < eCharY + 350*zoomAmount) {
						eHealth = eHealth - 0.25;
						bloodSfx = true;
						bloodX = eCharX + tempx;
						bloodY = yIn-eCharY;
						bloodloop = 0;
						if (faceDirection === "right") {
							bloodInv = 1;
						} else {
							bloodInv = 0;
						}
					}
				} else {
					if (yIn > eCharY && yIn < eCharY + 350*zoomAmount) {
						console.log("eCharY:"+eCharY+", yIn:"+yIn)
						eHealth = eHealth - 0.25;
						bloodSfx = true;
						bloodX = eCharX + tempx;
						bloodY = yIn-eCharY;
						bloodloop = 0;
						if (faceDirection === "right") {
							bloodInv = 1;
						} else {
							bloodInv = 0;
						}
					}
				}
			} else {
				var shotAngle = aimAngle*180/Math.PI + focusDelta*180/Math.PI*Math.random()*(1-(-1))+(-1);
				var k = Math.tan(shotAngle*Math.PI/180);
				if (standStatus === "crouch") {
					var y1 = (y + crouchingY[crouchloop]*zoomAmount);
					var x1 = (x + crouchingX[crouchloop]*zoomAmount);
				} else {
					var y1 = (y + (75+14)*zoomAmount);
					var x1 = (x + (185-7)*zoomAmount);
				}
				var yIn = k*(bgX-500 - x1) + y1;
				var yOut = k*(bgX + 3500 - x1) + y1;
				if (faceDirection === "left") {
					context.beginPath();
					context.moveTo(x1,y1);
					context.lineTo(bgX-500,yIn);
					context.strokeStyle = '#ffff00';
					context.stroke();
				} else {
					context.beginPath();
					context.moveTo(x1,y1);
					context.lineTo(bgX+3500,yOut);
					context.strokeStyle = '#ffff00';
					context.stroke();
				}
				for (i = 0; i < nOfEnemies; i = i + 1) {
					if (enemyArray[i][3] === 1) {
						var eX = enemyArray[i][1] + 100*Math.cos(1.3*(gameclock*30+frameclock)*Math.PI/180)+bgX+cameraX;
						var eY = enemyArray[i][2] + 100*Math.sin(1.3*(gameclock*30+frameclock)*Math.PI/180)+bgY+cameraY;
						
						if ((faceDirection === "left" && eX < pCharX) || (faceDirection === "right" && eX > pCharX)) {
							var yMid = k*(eX+90 - x1) + y1;
							var yMid2 = k*(eX+10 - x1) + y1;
							var yMid3 = k*(eX+50 - x1) + y1;
							
							if ((yMid > eY && yMid < eY + 100) || (yMid2 > eY && yMid2 < eY + 100) || (yMid3 > eY && yMid3 < eY + 100)) {
								for (j = 0; j < 20; j = j + 1) {
									if (j === 19) {
										soundDroid[0].currentTime = 0;
										soundDroid[0].play();
									}
									if (soundDroid[j].currentTime === 0) {
										soundDroid[j].play();
										break;
									}
								}
								enemyArray[i][3] = 0;
								killcount = killcount + 1;
								ammoValue = ammoValue + 3;
							}
						}
						
					}
					
				}
				
			}
			
		}
		
		/* GET BELOW GROUND Y */ 
		
		function getGroundY() {
			var mindx = [];
			var mindy = 2000;
			var tempy = 0;
			for (i = 0; i < maps[mapIndex][0].length; i = i + 1) {
				if (maps[mapIndex][0][i] < Math.abs(bgX - pCharX) && (maps[mapIndex][0][i]+maps[mapIndex][1][i]) > Math.abs(bgX - pCharX) ) {
					mindx.push(i);
				}
			}
			for (i = 0; i < mindx.length; i = i + 1) {
				if (Math.abs(bgY - pCharY)-maps[mapIndex][2][mindx[i]] < mindy && Math.abs(maps[mapIndex][2][mindx[i]]-220)-Math.abs(bgY-pCharY) > 0 ) {
					tempy = mindy;
					mindy = Math.min(mindy, bgY + maps[mapIndex][2][mindx[i]]);
					if (tempy !== mindy) {
						gindex = mindx[i];
					}
				}
			}
			groundY = mindy;
		}
		
		/* GEY CLOSEST ABOVE GROUND Y */
		
		function getUpY() {
			var mindx = [];
			var mindy = 2000;
			var tempy = 0;
			for (i = 0; i < maps[mapIndex][0].length; i = i + 1) {
				if (maps[mapIndex][0][i] < Math.abs(bgX - pCharX) && (maps[mapIndex][0][i]+maps[mapIndex][1][i]) > Math.abs(bgX - pCharX) ) {
					mindx.push(i);
				}
			}
			for (i = 0; i < mindx.length; i = i + 1) {
				if (Math.abs(bgY - pCharY)-maps[mapIndex][2][mindx[i]] < mindy /*&& Math.abs(maps[mapIndex][2][mindx[i]]-220)-Math.abs(bgY-pCharY) < 0 */) {
					
					
					if (bgY + maps[mapIndex][2][mindx[i]] > 0) {
						tempy = mindy;
						mindy = Math.min(mindy, bgY + maps[mapIndex][2][mindx[i]]);
						if (tempy !== mindy) {
							gindex = mindx[i];
						}
					}
				}
			}
			upY = mindy;
		}
		
		/* BG SCROLLER */ 
		
		function bgscroll() {
			if (keyPressed.includes("a")) {
				bgX = bgX + (15 + clientAgi*0.2)  * zoomAmount;
				if(bgX > c.width/2 - imagearray["invcharwalk"+walkloop].naturalWidth * zoomAmount / 2) {
					bgX = c.width/2 - imagearray["invcharwalk"+walkloop].naturalWidth * zoomAmount / 2;						
				}
				faceDirection = "left";
			} else if (keyPressed.includes("d")) {
				bgX = bgX - (15 + clientAgi*0.2)  * zoomAmount;
				if (bgX < -bg.naturalWidth + c.width/2 + imagearray["invcharwalk"+walkloop].naturalWidth*zoomAmount/2) {
					bgX = -bg.naturalWidth + c.width/2 + imagearray["invcharwalk"+walkloop].naturalWidth*zoomAmount/2;
				}
				faceDirection = "right";
			}
		}
		
		/* PVE MANAGEMENT */
		
		
		function pvedata() {
			/* SPAWN DROIDS BETWEEN REGULAR INTERVALS */
			if ((gameclock%5 === 0 && frameclock === 20) || (gameclock === 5 && frameclock === 20)) {
				spawnEnemy();
			}
			for (i = 0; i < nOfEnemies; i = i + 1) {
				if (enemyArray[i][3] === 1) {
					var eX = enemyArray[i][1] + 100*Math.cos(2*(gameclock*30+frameclock)*Math.PI/180);
					var eY = enemyArray[i][2] + 100*Math.sin(2*(gameclock*30+frameclock)*Math.PI/180);
					
					context.drawImage(imagearray["enemydroid"], bgX + eX + cameraX, bgY + eY + cameraY);
					
					/* DROID SHOOTS IF NEAR ENOUGH & IS LOADED */
					if (enemyArray[i][4] < 30) {
						enemyArray[i][4] = enemyArray[i][4] + 1;
					} else {
						
						if (Math.sqrt((pCharX+cameraX - (bgX + cameraX + eX))*(pCharX+cameraX - (bgX + cameraX + eX)) + (pCharY+cameraY - (eY + bgY + cameraY))*(pCharY+cameraY - (eY + bgY + cameraY))) < 400) {
							for (f = 0; f < 20; f = f + 1) {
								if (f === 19) {
									soundLaser[0].currentTime = 0;
									soundLaser[0].play();
								}
								if (soundLaser[f].currentTime === 0) {
									soundLaser[f].play();
									break;
								}
							}
							
							var tX = 0;
							var tY = 0;
							if (pCharX + imagearray["armlessstand"].naturalWidth*zoomAmount + cameraX < eX + bgX + cameraX) {
								tX = -1;
							} else if (pCharX +cameraX > eX + bgX + cameraX){
								tX = 1;
							}
							if (pCharY + cameraY + 350*zoomAmount > eY + cameraY + bgY) {
								tY = 1;
							} else if (pCharY + cameraY < eY + cameraY + bgY) {
								tX = -1;
							}
							shotArray[shotArray.length] = [nOfShots, eX, eY, 1, tX, tY];
							nOfShots = nOfShots + 1;
							enemyArray[i][4] = 0;
						}
					}
				}
			}
			for (i = 0; i < nOfShots; i = i + 1) {
				if (shotArray[i][3] === 1) {
					
					var projSpeed = 8;
					shotArray[i][1] = shotArray[i][1] + shotArray[i][4]*projSpeed;
					shotArray[i][2] = shotArray[i][2] + shotArray[i][5]*projSpeed;
					context.drawImage(imagearray["projectile"], bgX + shotArray[i][1] + cameraX, bgY + shotArray[i][2] + cameraY)
					if (aimStatus) {
						if ((bgX + cameraX + shotArray[i][1]+10 < cameraX - 150*zoomAmount + pCharX+imagearray["armlessstand"].naturalWidth*zoomAmount) && (bgX + cameraX + shotArray[i][1]+10 > cameraX + 150*zoomAmount + pCharX)) {
							if ((bgY + cameraY + shotArray[i][2]+10 > cameraY + pCharY) && (bgY + cameraY + shotArray[i][2]+10 < cameraY + pCharY + 350*zoomAmount)) {
								pHealth = pHealth - 0.25;
								shotArray[i][3] = 0;
							}
						}
					} else {
						if ((bgX + cameraX + shotArray[i][1]+10 < cameraX-100*zoomAmount + pCharX+imagearray["charwalk"+2].naturalWidth*zoomAmount) && (bgX + cameraX + shotArray[i][1]+10 > cameraX + 100*zoomAmount + pCharX)) {
							if ((bgY + cameraY + shotArray[i][2]+10 > cameraY + pCharY) && (bgY + cameraY + shotArray[i][2]+10 < cameraY + pCharY + 350*zoomAmount)) {
								pHealth = pHealth - 0.25;
								shotArray[i][3] = 0;
							}
						}
					}
				}
			}
		}
		/* SPAWN DROIDS */
		function spawnEnemy() {
			
			var enemyX = Math.floor(Math.random() * 2900) + 100  ;
			var enemyY = Math.floor(Math.random() * 1600) + 100  ;
			var enemyHealth = 1;
			var shotcounter = 0;
			enemy = [nOfEnemies, enemyX, enemyY, enemyHealth, shotcounter];
			enemyArray[enemyArray.length] = enemy;
			nOfEnemies = nOfEnemies + 1;
			
		}
		
		
		/* PVP DATA READER */
		
		function pvpdata() {/*  0 			1				2			3			4			5				6			7 		*/
			pDataArray = [bgX-pCharX, bgY-pCharY, mousePosX, mousePosY, socket.id, standStatus, faceDirection, aimStatus,
							walkloop, crouchloop, jumploop, gundrawloop, shootloop, shootStatus, rmbDown, x, y, eHealth, clientGamename, climbloop];
						/*		8			9		10			11			12			13			14    15  16   17  			18			19*/
			socket.emit("send message", pDataArray);
			pDataArray = [];
			/* SEND YOUR INFO ^ */
			
			/* GET ENEMY INFO */
			socket.on("new message", function(data) {
				if ((socket.id !== data.msg[4]) && (clientGamename === data.msg[18])) {
					eDataArray = data.msg;
					eCharX = bgX-eDataArray[0];
					eCharY = bgY-eDataArray[1];
					
					estandStatus = data.msg[5];
					efaceDirection = data.msg[6];
					eaimStatus = data.msg[7];
					emousePosX = data.msg[2];
					emousePosY = data.msg[3];
					tempwl = data.msg[8];
					tempcl = data.msg[9];
					tempjl = data.msg[10];
					egundrawloop = data.msg[11];
					eshootloop = data.msg[12];
					eshootStatus = data.msg[13];
					ermbDown = data.msg[14];
					ex = data.msg[15];
					ey = data.msg[16];
					pHealth = data.msg[17];
					eclimbloop = data.msg[19];
				}
			});
			/* SHOOTING SOUNDS */
			if (eshootStatus === true && eshootloop === 0) {
				for (i = 0; i < 20; i = i + 1) {
					if (i === 19) {
						soundShot[0].currentTime = 0;
						soundShot[0].play();
					}
					if (soundShot[i].currentTime === 0) {
						soundShot[i].play();
						break;
					}
				}
			}
			
			for (i = 0; i < 20; i = i + 1) {
				if (soundShot[i].currentTime > 0.6) {
					soundShot[i].currentTime = 0;
					soundShot[i].pause();
					for (f = 0; f < 20; f = f + 1) {
						if (f === 19) {
							soundShell[0].currentTime = 0;
							soundShell[0].play();
						}
						if (soundShell[f].currentTime === 0) {
							soundShell[f].play();
							break;
						}
					}
				}
			}
			
			if (bloodSfx) {
				console.log(eCharY, bloodY)
				if (bloodInv === 1) {
					context.drawImage(imagearray["invsfxblood"+bloodloop], bloodX + cameraX, eCharY + bloodY-60);
				} else {
					context.drawImage(imagearray["sfxblood"+bloodloop], bloodX + cameraX - 120, eCharY + bloodY-60);
				}
				bloodloop = bloodloop + 1;
				if (bloodloop > 5) {
					bloodloop = 0;
					bloodSfx = false;
				}
			}
			
			/* DRAW ENEMY */ 
			
			if (estandStatus === "stand") {
				if (eaimStatus) {
					if (egundrawloop < 11) {
						eshootStatus = false;
						if (ermbDown) {
							if (efaceDirection === "left") {
								context.drawImage(imagearray["armlessstand"] ,eCharX ,eCharY , imagearray["armlessstand"].naturalWidth * zoomAmount, imagearray["armlessstand"].naturalHeight * zoomAmount)
								context.drawImage(imagearray["gunarms"+egundrawloop],eCharX ,eCharY , imagearray["gunarms"+egundrawloop].naturalWidth * zoomAmount, imagearray["gunarms"+egundrawloop].naturalHeight * zoomAmount);
							} else {
								context.drawImage(imagearray["invarmlessstand"],eCharX ,eCharY , imagearray["invarmlessstand"].naturalWidth * zoomAmount, imagearray["invarmlessstand"].naturalHeight * zoomAmount)
								context.drawImage(imagearray["invgunarms"+egundrawloop],eCharX ,eCharY , imagearray["invgunarms"+egundrawloop].naturalWidth * zoomAmount, imagearray["invgunarms"+egundrawloop].naturalHeight * zoomAmount);
							}
						} else {
							if (efaceDirection === "left") {
								context.drawImage(imagearray["armlessstand"],eCharX ,eCharY , imagearray["armlessstand"].naturalWidth * zoomAmount, imagearray["armlessstand"].naturalHeight * zoomAmount)
								context.drawImage(imagearray["gunarmsreverse"+egundrawloop],eCharX ,eCharY , imagearray["gunarmsreverse"+egundrawloop].naturalWidth * zoomAmount, imagearray["gunarmsreverse"+egundrawloop].naturalHeight * zoomAmount);
							} else {
								context.drawImage(imagearray["invarmlessstand"],eCharX ,eCharY , imagearray["invarmlessstand"].naturalWidth * zoomAmount, imagearray["invarmlessstand"].naturalHeight * zoomAmount)
								context.drawImage(imagearray["invgunarmsreverse"+egundrawloop],eCharX ,eCharY , imagearray["invgunarmsreverse"+egundrawloop].naturalWidth * zoomAmount, imagearray["invgunarmsreverse"+egundrawloop].naturalHeight * zoomAmount);
							}
						}
					} else {
					
						/* MOVING WHILE AIMING */
						if (ewalkloop !== tempwl) {
							ewalkloop = tempwl;
							if (efaceDirection === "right") {
								context.drawImage(imagearray["invarmlesscharwalk"+ewalkloop],eCharX, eCharY , imagearray["invarmlesscharwalk"+ewalkloop].naturalWidth * zoomAmount, imagearray["invarmlesscharwalk"+ewalkloop].naturalHeight * zoomAmount);
							} else {
								context.drawImage(imagearray["armlesscharwalk"+ewalkloop],eCharX, eCharY , imagearray["armlesscharwalk"+ewalkloop].naturalWidth * zoomAmount, imagearray["armlesscharwalk"+ewalkloop].naturalHeight * zoomAmount);
							}
							if (efaceDirection === "left") {
								context.drawImage(imagearray["armlesscharwalk"+ewalkloop],eCharX, eCharY , imagearray["armlesscharwalk"+ewalkloop].naturalWidth * zoomAmount, imagearray["armlesscharwalk"+ewalkloop].naturalHeight * zoomAmount);
							} else {
								context.drawImage(imagearray["invarmlesscharwalk"+ewalkloop],eCharX, eCharY , imagearray["invarmlesscharwalk"+ewalkloop].naturalWidth * zoomAmount, imagearray["invarmlesscharwalk"+ewalkloop].naturalHeight * zoomAmount);
							}
						} else {
							if (efaceDirection === "left") {
								context.drawImage(imagearray["armlessstand"],eCharX, eCharY , imagearray["armlessstand"].naturalWidth * zoomAmount, imagearray["armlessstand"].naturalHeight * zoomAmount);
							} else {
								context.drawImage(imagearray["invarmlessstand"],eCharX, eCharY , imagearray["invarmlessstand"].naturalWidth * zoomAmount, imagearray["invarmlessstand"].naturalHeight * zoomAmount);
							}
						}
						
						/* HAND ANIMATION */
						
						if (efaceDirection === "left") {
							/* MOUSE DISTANCE FROM CHAR (75+14 ja 185-7 = SHOULDERCOORDINATES)*/
							mouseDistance = Math.sqrt((emousePosY - (ey +(75+14)*zoomAmount))*(emousePosY - (ey +(75+14)*zoomAmount))+(emousePosX - (ex + (185-7)*zoomAmount))*(emousePosX - (ex + (185-7)*zoomAmount)));
							
							/* FIX ROTATION ERROR */
							aimCorrectA = 70 * zoomAmount;
							aimCorrectB = mouseDistance - Math.sqrt((233 * zoomAmount)*(233 * zoomAmount) + (aimCorrectA)*(aimCorrectA));
							aimCorrectG = 180 - Math.atan((aimCorrectA)/(233*zoomAmount))*180/Math.PI;
							aimCorrectC = Math.sqrt(aimCorrectA*aimCorrectA + aimCorrectB*aimCorrectB - 2*aimCorrectA*aimCorrectB*Math.cos(aimCorrectG));
							aimCorrect = (aimCorrectB*aimCorrectB + aimCorrectC*aimCorrectC - aimCorrectA*aimCorrectA)/(2*aimCorrectB*aimCorrectC);
							
							/* AIMANGLE */
							aimAngle = (Math.atan((emousePosY - (ey + (75+14)*zoomAmount))/(emousePosX - (ex + (185-7)*zoomAmount))))+aimCorrect*Math.PI/180;

							context.save();
							context.translate(eCharX+(185-7)*zoomAmount, eCharY+(75+14)*zoomAmount);
							context.rotate(aimAngle);
							
							/* SHOOTLOOP */
							
							if (eshootloop > 9) {
								eshootStatus = false;
							}
							
							if (eshootStatus) {
								context.drawImage(imagearray["pistolshoot"+eshootloop],0-233*zoomAmount,0-88*zoomAmount , imagearray["pistolshoot"+eshootloop].naturalWidth * zoomAmount, imagearray["pistolshoot"+eshootloop].naturalHeight * zoomAmount);
							} else {
								context.drawImage(imagearray["gunarm"],0-233*zoomAmount,0-88*zoomAmount , imagearray["gunarm"].naturalWidth * zoomAmount, imagearray["gunarm"].naturalHeight * zoomAmount);
							}
						
							context.restore();
						} else {
							/* MOUSE DISTANCE FROM CHAR (75-16 ja 185+14 = SHOULDERCOORDINATES)*/
							mouseDistance = Math.sqrt((emousePosY - (ey +(75+14)*zoomAmount))*(emousePosY - (ey +(75+14)*zoomAmount))+(emousePosX - (ex + (185+14)*zoomAmount))*(emousePosX - (ex + (185+14)*zoomAmount)));
							
							/* FIX ROTATION ERROR */
							aimCorrectA = 70 * zoomAmount;
							aimCorrectB = mouseDistance - Math.sqrt((233 * zoomAmount)*(233 * zoomAmount) + (aimCorrectA)*(aimCorrectA));
							aimCorrectG = 180 - Math.atan((aimCorrectA)/(233*zoomAmount))*180/Math.PI;
							aimCorrectC = Math.sqrt(aimCorrectA*aimCorrectA + aimCorrectB*aimCorrectB - 2*aimCorrectA*aimCorrectB*Math.cos(aimCorrectG));
							aimCorrect = (aimCorrectB*aimCorrectB + aimCorrectC*aimCorrectC - aimCorrectA*aimCorrectA)/(2*aimCorrectB*aimCorrectC);
							
							/* AIMANGLE */
							aimAngle = (Math.atan((emousePosY - (ey + (75+14)*zoomAmount))/(emousePosX - (ex + (185+14)*zoomAmount))))-aimCorrect*Math.PI/180;

							context.save();
							context.translate(eCharX+(185+16)*zoomAmount, eCharY+(75+14)*zoomAmount);
							context.rotate(aimAngle);
							
							/* SHOOTLOOP */
							
							if (eshootloop > 9) {
								eshootStatus = false;
							}
							
							if (eshootStatus) {
								context.drawImage(imagearray["invpistolshoot"+eshootloop],0-27*zoomAmount,0-88*zoomAmount , imagearray["invpistolshoot"+eshootloop].naturalWidth * zoomAmount, imagearray["invpistolshoot"+eshootloop].naturalHeight * zoomAmount)
							} else {
								context.drawImage(imagearray["invgunarm"],0-27*zoomAmount,0-88*zoomAmount , imagearray["invgunarm"].naturalWidth * zoomAmount, imagearray["invgunarm"].naturalHeight * zoomAmount);
							}
							context.restore();
						}
					}
				} else {
					if (efaceDirection === "left") {
						if (ewalkloop !== tempwl) {
							ewalkloop = tempwl;
							context.drawImage(imagearray["charwalk"+ewalkloop], eCharX + cameraX , eCharY + cameraY , imagearray["charwalk"+ewalkloop].naturalWidth*zoomAmount, imagearray["charwalk"+ewalkloop].naturalHeight*zoomAmount);
						} else {
							context.drawImage(imagearray["charstand1"], eCharX + cameraX , eCharY + cameraY , imagearray["charstand1"].naturalWidth*zoomAmount, imagearray["charstand1"].naturalHeight*zoomAmount);
						}
					} else {
						if (ewalkloop !== tempwl) {
							ewalkloop = tempwl;
							context.drawImage(imagearray["invcharwalk"+ewalkloop], eCharX + cameraX , eCharY + cameraY , imagearray["invcharwalk"+ewalkloop].naturalWidth*zoomAmount, imagearray["invcharwalk"+ewalkloop].naturalHeight*zoomAmount);
						} else {
							context.drawImage(imagearray["invcharstand1"], eCharX + cameraX , eCharY + cameraY , imagearray["invcharstand1"].naturalWidth*zoomAmount, imagearray["invcharstand1"].naturalHeight*zoomAmount);
						}
					}
				}
				/* CROUCH */
			} else if (estandStatus === "crouch") {
				if (eaimStatus) {
					if (egundrawloop < 11) {
						if (ermbDown) {
							eshootStatus = false;
							if (efaceDirection === "left") {
								context.drawImage(imagearray["armlesscharcrouch"+ecrouchloop],eCharX ,eCharY , imagearray["armlesscharcrouch"+ecrouchloop].naturalWidth * zoomAmount, imagearray["armlesscharcrouch"+ecrouchloop].naturalHeight * zoomAmount);
								context.drawImage(imagearray["gunarms"+egundrawloop],eCharX + midcrouchingX[ecrouchloop] * zoomAmount ,eCharY + midcrouchingY[ecrouchloop] * zoomAmount , imagearray["gunarms"+egundrawloop].naturalWidth * zoomAmount, imagearray["gunarms"+egundrawloop].naturalHeight * zoomAmount);
							} else {
								context.drawImage(imagearray["invarmlesscharcrouch"+ecrouchloop],eCharX ,eCharY , imagearray["invarmlesscharcrouch"+crouchloop].naturalWidth * zoomAmount, imagearray["invarmlesscharcrouch"+crouchloop].naturalHeight * zoomAmount)
								context.drawImage(imagearray["invgunarms"+egundrawloop],eCharX + invmidcrouchingX[ecrouchloop] * zoomAmount ,eCharY + invmidcrouchingY[ecrouchloop] * zoomAmount , imagearray["invgunarms"+gundrawloop].naturalWidth * zoomAmount, imagearray["invgunarms"+gundrawloop].naturalHeight * zoomAmount);
							}
						} else {
							if (efaceDirection === "left") {
								context.drawImage(imagearray["armlesscharcrouch"+ecrouchloop],eCharX ,eCharY , imagearray["armlesscharcrouch"+ecrouchloop].naturalWidth * zoomAmount, imagearray["armlesscharcrouch"+ecrouchloop].naturalHeight * zoomAmount)
								context.drawImage(imagearray["gunarmsreverse"+egundrawloop],eCharX + midcrouchingX[ecrouchloop] * zoomAmount ,eCharY + midcrouchingY[ecrouchloop] * zoomAmount , imagearray["gunarmsreverse"+gundrawloop].naturalWidth * zoomAmount, imagearray["gunarmsreverse"+gundrawloop].naturalHeight * zoomAmount);
							} else {
								context.drawImage(imagearray["invarmlesscharcrouch"+ecrouchloop],eCharX ,eCharY , imagearray["invarmlesscharcrouch"+ecrouchloop].naturalWidth * zoomAmount, imagearray["invarmlesscharcrouch"+crouchloop].naturalHeight * zoomAmount)
								context.drawImage(imagearray["invgunarmsreverse"+egundrawloop],eCharX + invmidcrouchingX[ecrouchloop] * zoomAmount ,eCharY + invmidcrouchingY[ecrouchloop] * zoomAmount , imagearray["invgunarmsreverse"+gundrawloop].naturalWidth * zoomAmount, imagearray["invgunarmsreverse"+gundrawloop].naturalHeight * zoomAmount);
							}
						}
					} else {
						ecrouchloop = tempcl;
						if (efaceDirection === "left") {
							context.drawImage(imagearray["armlesscharcrouch"+ecrouchloop],eCharX ,eCharY , imagearray["armlesscharcrouch"+ecrouchloop].naturalWidth * zoomAmount, imagearray["armlesscharcrouch"+ecrouchloop].naturalHeight * zoomAmount);
						} else {
							context.drawImage(imagearray["invarmlesscharcrouch"+ecrouchloop],eCharX ,eCharY,  imagearray["invarmlesscharcrouch"+ecrouchloop].naturalWidth * zoomAmount, imagearray["invarmlesscharcrouch"+crouchloop].naturalHeight * zoomAmount);
						}
							
					
						/* HANDANIMATION */
					
						if (efaceDirection === "left") {
							/* MOUSE DIST FROM CHAR (75-16 ja 185+14 = SHOUDLERCOORDINATES)*/
							mouseDistance = Math.sqrt((emousePosY - (ey + crouchingY[ecrouchloop]*zoomAmount))*(emousePosY - (ey + crouchingY[ecrouchloop]*zoomAmount))+(emousePosX - (ex + crouchingX[ecrouchloop]*zoomAmount))*(emousePosX - (ex + crouchingX[ecrouchloop]*zoomAmount)));
							
							/* FIX ROTATION ERROR */
							aimCorrectA = 70 * zoomAmount;
							aimCorrectB = mouseDistance - Math.sqrt((233 * zoomAmount)*(233 * zoomAmount) + (aimCorrectA)*(aimCorrectA));
							aimCorrectG = 180 - Math.atan((aimCorrectA)/(233*zoomAmount))*180/Math.PI;
							aimCorrectC = Math.sqrt(aimCorrectA*aimCorrectA + aimCorrectB*aimCorrectB - 2*aimCorrectA*aimCorrectB*Math.cos(aimCorrectG));
							aimCorrect = (aimCorrectB*aimCorrectB + aimCorrectC*aimCorrectC - aimCorrectA*aimCorrectA)/(2*aimCorrectB*aimCorrectC);
							
							/* AIMANGLE */
							aimAngle = (Math.atan((emousePosY - (ey + crouchingY[ecrouchloop]*zoomAmount))/(emousePosX - (ex + crouchingX[ecrouchloop]*zoomAmount))))+aimCorrect*Math.PI/180;
					
							context.save();
							context.translate(eCharX + crouchingX[ecrouchloop]* zoomAmount, eCharY + crouchingY[ecrouchloop]* zoomAmount);
							context.rotate(aimAngle);
	
							if (eshootloop > 9) {
								eshootStatus = false;
							}
							
							if (eshootStatus) {
								context.drawImage(imagearray["pistolshoot"+eshootloop],0-233*zoomAmount,0-88*zoomAmount , imagearray["pistolshoot"+eshootloop].naturalWidth * zoomAmount, imagearray["pistolshoot"+eshootloop].naturalHeight * zoomAmount);
							} else {
								context.drawImage(imagearray["gunarm"],0-233*zoomAmount,0-88*zoomAmount , imagearray["gunarm"].naturalWidth * zoomAmount, imagearray["gunarm"].naturalHeight * zoomAmount);
							}
							
							context.restore();
						
						} else {
							/* MOUSE DIST FROM CHAR (75-16 ja 185+14 = SHOULDERCOORDINATES)*/
							mouseDistance = Math.sqrt((emousePosY - (ey + invcrouchingY[ecrouchloop]*zoomAmount))*(emousePosY - (ey + invcrouchingY[ecrouchloop]*zoomAmount))+(emousePosX - (ex + invcrouchingX[ecrouchloop]*zoomAmount))*(emousePosX - (ex + invcrouchingX[ecrouchloop]*zoomAmount)));
							
							/* FIX ROTATION ERROR */
							aimCorrectA = 70 * zoomAmount;
							aimCorrectB = mouseDistance - Math.sqrt((233 * zoomAmount)*(233 * zoomAmount) + (aimCorrectA)*(aimCorrectA));
							aimCorrectG = 180 - Math.atan((aimCorrectA)/(233*zoomAmount))*180/Math.PI;
							aimCorrectC = Math.sqrt(aimCorrectA*aimCorrectA + aimCorrectB*aimCorrectB - 2*aimCorrectA*aimCorrectB*Math.cos(aimCorrectG));
							aimCorrect = (aimCorrectB*aimCorrectB + aimCorrectC*aimCorrectC - aimCorrectA*aimCorrectA)/(2*aimCorrectB*aimCorrectC);
							
							/* AIMANGLE */
							aimAngle = (Math.atan((emousePosY - (ey + invcrouchingY[ecrouchloop]*zoomAmount))/(emousePosX - (ex + invcrouchingX[ecrouchloop]*zoomAmount))))-aimCorrect*Math.PI/180;
					
							context.save();
							context.translate(eCharX + invcrouchingX[ecrouchloop]* zoomAmount, eCharY + invcrouchingY[ecrouchloop]* zoomAmount);
							context.rotate(aimAngle);
							
							if (eshootloop > 9) {
								eshootStatus = false;
							}
							
							if (eshootStatus) {
								context.drawImage(imagearray["invpistolshoot"+eshootloop],0-27*zoomAmount,0-88*zoomAmount , imagearray["invpistolshoot"+eshootloop].naturalWidth * zoomAmount, imagearray["invpistolshoot"+eshootloop].naturalHeight * zoomAmount)
							} else {
								context.drawImage(imagearray["invgunarm"],0-27*zoomAmount,0-88*zoomAmount , imagearray["invgunarm"].naturalWidth * zoomAmount, imagearray["invgunarm"].naturalHeight * zoomAmount);
							}
							
							context.restore();
						}
					}
				} else {
					ecrouchloop = tempcl;
					if (efaceDirection === "left") {
						context.drawImage(imagearray["charcrouch"+ecrouchloop], eCharX + cameraX , eCharY + cameraY , imagearray["charcrouch"+ecrouchloop].naturalWidth*zoomAmount, imagearray["charcrouch"+ecrouchloop].naturalHeight*zoomAmount);
					} else {
						context.drawImage(imagearray["invcharcrouch"+ecrouchloop], eCharX + cameraX , eCharY + cameraY , imagearray["invcharcrouch"+ecrouchloop].naturalWidth*zoomAmount, imagearray["invcharcrouch"+ecrouchloop].naturalHeight*zoomAmount);
					}
				}
				/* JUMP */
			} else if (estandStatus === "air") {
				ejumploop = tempjl;
				if (efaceDirection === "left") {
					context.drawImage(imagearray["jump"+ejumploop],eCharX , eCharY, imagearray["jump"+ejumploop].naturalWidth * zoomAmount, imagearray["jump"+ejumploop].naturalHeight * zoomAmount);
				} else {
					context.drawImage(imagearray["invjump"+ejumploop],eCharX , eCharY , imagearray["invjump"+ejumploop].naturalWidth * zoomAmount, imagearray["invjump"+ejumploop].naturalHeight * zoomAmount);
				}
				/* CLIMB */
			} else if (estandStatus === "climb") {
				if (efaceDirection === "left") {
					context.drawImage(imagearray["jumpclimbtest"+eclimbloop], eCharX, eCharY, imagearray["jumpclimbtest"+eclimbloop].naturalWidth * zoomAmount, imagearray["jumpclimbtest"+eclimbloop].naturalHeight * zoomAmount);
				} else {
					context.drawImage(imagearray["invjumpclimbtest"+eclimbloop], eCharX, eCharY, imagearray["invjumpclimbtest"+eclimbloop].naturalWidth * zoomAmount, imagearray["invjumpclimbtest"+eclimbloop].naturalHeight * zoomAmount);
				}	
			}
			
		}

	}
	
	
	
});